package cd.forexy.service;

import cd.forexy.domain.UserDetailsEntity;
import cd.forexy.domain.security.UserEntity;

import java.io.Serializable;
import java.util.List;

import javax.inject.Named;
import javax.persistence.PersistenceUnitUtil;
import javax.transaction.Transactional;

@Named
public class UserDetailsService extends BaseService<UserDetailsEntity> implements Serializable {

    private static final long serialVersionUID = 1L;
    
    public UserDetailsService(){
        super(UserDetailsEntity.class);
    }
    
    @Transactional
    public List<UserDetailsEntity> findAllUserDetailsEntities() {
        
        return entityManager.createQuery("SELECT o FROM UserDetails o LEFT JOIN FETCH o.image", UserDetailsEntity.class).getResultList();
    }
    
    @Override
    @Transactional
    public long countAllEntries() {
        return entityManager.createQuery("SELECT COUNT(o) FROM UserDetails o", Long.class).getSingleResult();
    }
    
    @Override
    protected void handleDependenciesBeforeDelete(UserDetailsEntity userDetails) {

        /* This is called before a UserDetails is deleted. Place here all the
           steps to cut dependencies to other entities */
        
        this.cutAllDetailsUsersAssignments(userDetails);
        
    }

    // Remove all assignments from all user a userDetails. Called before delete a userDetails.
    @Transactional
    private void cutAllDetailsUsersAssignments(UserDetailsEntity userDetails) {
        entityManager
                .createQuery("UPDATE User c SET c.details = NULL WHERE c.details = :p")
                .setParameter("p", userDetails).executeUpdate();
    }
    
    // Find all user which are not yet assigned to a userDetails
    @Transactional
    public List<UserDetailsEntity> findAvailableDetails(UserEntity user) {
        Long id = -1L;
        if (user != null && user.getDetails() != null && user.getDetails().getId() != null) {
            id = user.getDetails().getId();
        }
        return entityManager.createQuery(
                "SELECT o FROM UserDetails o where o.id NOT IN (SELECT p.details.id FROM User p where p.details.id != :id)", UserDetailsEntity.class)
                .setParameter("id", id).getResultList();    
    }

    @Transactional
    public UserDetailsEntity lazilyLoadImageToUserDetails(UserDetailsEntity userDetails) {
        PersistenceUnitUtil u = entityManager.getEntityManagerFactory().getPersistenceUnitUtil();
        if (!u.isLoaded(userDetails, "image") && userDetails.getId() != null) {
            userDetails = find(userDetails.getId());
            userDetails.getImage().getId();
        }
        return userDetails;
    }
    
}
