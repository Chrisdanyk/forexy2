package cd.forexy.service;

import cd.forexy.domain.AgenceEntity;

import java.io.Serializable;
import java.util.List;

import javax.inject.Named;
import javax.transaction.Transactional;

@Named
public class AgenceService extends BaseService<AgenceEntity> implements Serializable {

    private static final long serialVersionUID = 1L;
    
    public AgenceService(){
        super(AgenceEntity.class);
    }
    
    @Transactional
    public List<AgenceEntity> findAllAgenceEntities() {
        
        return entityManager.createQuery("SELECT o FROM Agence o ", AgenceEntity.class).getResultList();
    }
    
    @Override
    @Transactional
    public long countAllEntries() {
        return entityManager.createQuery("SELECT DISTINCT COUNT(o) FROM Agence o", Long.class).getSingleResult();
    }
    
    @Override
    protected void handleDependenciesBeforeDelete(AgenceEntity agence) {

        /* This is called before a Agence is deleted. Place here all the
           steps to cut dependencies to other entities */
        
        this.cutAllAgenceUsersAssignments(agence);
        
        this.cutAllAgenceMouvementsAssignments(agence);
        
        this.cutAllAgenceContratsAssignments(agence);
        
        this.cutAllAgence2Contrat2sAssignments(agence);
        
        this.cutAllAgenceRetraitsAssignments(agence);
        
    }

    // Remove all assignments from all user a agence. Called before delete a agence.
    @Transactional
    private void cutAllAgenceUsersAssignments(AgenceEntity agence) {
        entityManager
                .createQuery("UPDATE User c SET c.agence = NULL WHERE c.agence = :p")
                .setParameter("p", agence).executeUpdate();
    }
    
    // Remove all assignments from all mouvement a agence. Called before delete a agence.
    @Transactional
    private void cutAllAgenceMouvementsAssignments(AgenceEntity agence) {
        entityManager
                .createQuery("UPDATE Mouvement c SET c.agence = NULL WHERE c.agence = :p")
                .setParameter("p", agence).executeUpdate();
    }
    
    // Remove all assignments from all contrat a agence. Called before delete a agence.
    @Transactional
    private void cutAllAgenceContratsAssignments(AgenceEntity agence) {
        entityManager
                .createQuery("UPDATE Contrat c SET c.agence = NULL WHERE c.agence = :p")
                .setParameter("p", agence).executeUpdate();
    }
    
    // Remove all assignments from all contrat2 a agence. Called before delete a agence.
    @Transactional
    private void cutAllAgence2Contrat2sAssignments(AgenceEntity agence) {
        entityManager
                .createQuery("UPDATE Contrat c SET c.agence2 = NULL WHERE c.agence2 = :p")
                .setParameter("p", agence).executeUpdate();
    }
    
    // Remove all assignments from all retrait a agence. Called before delete a agence.
    @Transactional
    private void cutAllAgenceRetraitsAssignments(AgenceEntity agence) {
        entityManager
                .createQuery("UPDATE Retrait c SET c.agence = NULL WHERE c.agence = :p")
                .setParameter("p", agence).executeUpdate();
    }
    
}
