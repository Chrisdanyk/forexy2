package cd.forexy.service;

import cd.forexy.domain.ParameterName;
import cd.forexy.domain.ParametreEntity;
import cd.forexy.service.security.SecurityWrapper;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Named;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.primefaces.model.SortOrder;

@Named
public class ParametreService extends BaseService<ParametreEntity> implements Serializable {

    private static final long serialVersionUID = 1L;

    public ParametreService() {
        super(ParametreEntity.class);
    }

    @Transactional
    public List<ParametreEntity> findAllParametreEntities() {

        return entityManager.createQuery("SELECT o FROM Parametre o ", ParametreEntity.class).getResultList();
    }

    @Override
    @Transactional
    public long countAllEntries() {
        return entityManager.createQuery("SELECT COUNT(o) FROM Parametre o", Long.class).getSingleResult();
    }

    @Override
    @Transactional
    public ParametreEntity save(ParametreEntity parametre) {
        String username = SecurityWrapper.getUsername();

        parametre.updateAuditInformation(username);

        return super.save(parametre);
    }

    @Override
    @Transactional
    public ParametreEntity update(ParametreEntity parametre) {
        String username = SecurityWrapper.getUsername();
        parametre.updateAuditInformation(username);
        return super.update(parametre);
    }

    @Override
    protected void handleDependenciesBeforeDelete(ParametreEntity parametre) {

        /* This is called before a Parametre is deleted. Place here all the
           steps to cut dependencies to other entities */

    }

    // This is the central method called by the DataTable
    @Override
    @Transactional
    public List<ParametreEntity> findEntriesPagedAndFilteredAndSorted(int firstResult, int maxResults, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

        StringBuilder query = new StringBuilder();

        query.append("SELECT o FROM Parametre o");

        String nextConnective = " WHERE";

        Map<String, Object> queryParameters = new HashMap<>();

        if (filters != null && !filters.isEmpty()) {

            nextConnective += " ( ";

            for (String filterProperty : filters.keySet()) {

                if (filters.get(filterProperty) == null) {
                    continue;
                }

                switch (filterProperty) {

                    case "date":
                        query.append(nextConnective).append(" o.date = :date");
                        queryParameters.put("date", filters.get(filterProperty));
                        break;

                    case "nom":
                        query.append(nextConnective).append(" o.nom LIKE :nom");
                        queryParameters.put("nom", "%" + filters.get(filterProperty) + "%");
                        break;

                    case "valeur":
                        query.append(nextConnective).append(" o.valeur LIKE :valeur");
                        queryParameters.put("valeur", "%" + filters.get(filterProperty) + "%");
                        break;

                    case "description":
                        query.append(nextConnective).append(" o.description LIKE :description");
                        queryParameters.put("description", "%" + filters.get(filterProperty) + "%");
                        break;

                }

                nextConnective = " AND";
            }

            query.append(" ) ");
            nextConnective = " AND";
        }

        if (sortField != null && !sortField.isEmpty()) {
            query.append(" ORDER BY o.").append(sortField);
            query.append(SortOrder.DESCENDING.equals(sortOrder) ? " DESC" : " ASC");
        }

        TypedQuery<ParametreEntity> q = this.entityManager.createQuery(query.toString(), this.getType());

        for (String queryParameter : queryParameters.keySet()) {
            q.setParameter(queryParameter, queryParameters.get(queryParameter));
        }

        return q.setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }

    public String getValueConfig(ParameterName name) {
        List<ParametreEntity> resultList = entityManager.createQuery("SELECT o FROM Parametre o where o.nom = :name", ParametreEntity.class)
                .setParameter("name", name).getResultList();
        if (resultList.isEmpty()) {
            return "";
        } else {
            return resultList.get(0).getValeur();
        }
    }

}
