package cd.forexy.service;

import cd.forexy.domain.*;
import cd.forexy.domain.security.UserEntity;
import cd.forexy.service.security.SecurityWrapper;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.*;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.PersistenceUnitUtil;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.primefaces.model.SortOrder;

@Named
public class ContratService extends BaseService<ContratEntity> implements Serializable {

    private static final long serialVersionUID = 1L;

    public ContratService() {
        super(ContratEntity.class);
    }

    @Inject
    private ContratAttachmentService attachmentService;

    @Transactional
    public List<ContratEntity> findAllContratEntities() {

        return entityManager.createQuery("SELECT o FROM Contrat o ", ContratEntity.class).getResultList();
    }

    @Override
    @Transactional
    public long countAllEntries() {
        return entityManager.createQuery("SELECT DISTINCT COUNT(o) FROM Contrat o", Long.class).getSingleResult();
    }

    @Override
    @Transactional
    public ContratEntity save(ContratEntity contrat) {
        String username = SecurityWrapper.getUsername();

        contrat.updateAuditInformation(username);

        return super.save(contrat);
    }

    @Override
    @Transactional
    public ContratEntity update(ContratEntity contrat) {
        String username = SecurityWrapper.getUsername();
        contrat.updateAuditInformation(username);
        return super.update(contrat);
    }

    @Override
    protected void handleDependenciesBeforeDelete(ContratEntity contrat) {

        /* This is called before a Contrat is deleted. Place here all the
           steps to cut dependencies to other entities */

        this.attachmentService.deleteAttachmentsByContrat(contrat);

        this.cutAllContratRetraitsAssignments(contrat);

    }

    // Remove all assignments from all retrait a contrat. Called before delete a contrat.
    @Transactional
    private void cutAllContratRetraitsAssignments(ContratEntity contrat) {
        entityManager
                .createQuery("UPDATE Retrait c SET c.contrat = NULL WHERE c.contrat = :p")
                .setParameter("p", contrat).executeUpdate();
    }

    @Transactional
    public List<ContratEntity> findAvailableContrats(AgenceEntity agence) {
        return entityManager.createQuery("SELECT o FROM Contrat o WHERE o.agence IS NULL", ContratEntity.class).getResultList();
    }

    @Transactional
    public List<ContratEntity> findContratsByAgence(AgenceEntity agence) {
        return entityManager.createQuery("SELECT o FROM Contrat o WHERE o.agence = :agence", ContratEntity.class).setParameter("agence", agence).getResultList();
    }

    @Transactional
    public List<ContratEntity> findAvailableContrats(UserEntity user) {
        return entityManager.createQuery("SELECT o FROM Contrat o WHERE o.user IS NULL", ContratEntity.class).getResultList();
    }

    @Transactional
    public List<ContratEntity> findContratsByUser(UserEntity user) {
        return entityManager.createQuery("SELECT o FROM Contrat o WHERE o.user = :user", ContratEntity.class).setParameter("user", user).getResultList();
    }

    @Transactional
    public List<ContratEntity> findAvailableContrat2s(AgenceEntity agence) {
        return entityManager.createQuery("SELECT o FROM Contrat o WHERE o.agence2 IS NULL", ContratEntity.class).getResultList();
    }

    @Transactional
    public List<ContratEntity> findContrat2sByAgence2(AgenceEntity agence) {
        return entityManager.createQuery("SELECT o FROM Contrat o WHERE o.agence2 = :agence", ContratEntity.class).setParameter("agence", agence).getResultList();
    }

    @Transactional
    public ContratEntity lazilyLoadImageToContrat(ContratEntity contrat) {
        PersistenceUnitUtil u = entityManager.getEntityManagerFactory().getPersistenceUnitUtil();
        if (!u.isLoaded(contrat, "image") && contrat.getId() != null) {
            contrat = find(contrat.getId());
            contrat.getImage().getId();
        }
        return contrat;
    }

    // This is the central method called by the DataTable
    @Override
    @Transactional
    public List<ContratEntity> findEntriesPagedAndFilteredAndSorted(int firstResult, int maxResults, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

        StringBuilder query = new StringBuilder();

        query.append("SELECT o FROM Contrat o");

        // Can be optimized: We need this join only when agence filter is set
        query.append(" LEFT OUTER JOIN o.agence agence");

        // Can be optimized: We need this join only when user filter is set
        query.append(" LEFT OUTER JOIN o.user user");

        // Can be optimized: We need this join only when agence2 filter is set
        query.append(" LEFT OUTER JOIN o.agence2 agence2");

        query.append(" LEFT JOIN FETCH o.image");

        String nextConnective = " WHERE";

        Map<String, Object> queryParameters = new HashMap<>();

        if (filters != null && !filters.isEmpty()) {

            nextConnective += " ( ";

            for (String filterProperty : filters.keySet()) {

                if (filters.get(filterProperty) == null) {
                    continue;
                }

                switch (filterProperty) {

                    case "civilite":
                        query.append(nextConnective).append(" o.civilite = :civilite");
                        queryParameters.put("civilite", ContratCivilite.valueOf(filters.get(filterProperty).toString()));
                        break;

                    case "piece":
                        query.append(nextConnective).append(" o.piece = :piece");
                        queryParameters.put("piece", ContratPiece.valueOf(filters.get(filterProperty).toString()));
                        break;

                    case "montant":
                        query.append(nextConnective).append(" o.montant = :montant");
                        queryParameters.put("montant", new BigDecimal(filters.get(filterProperty).toString()));
                        break;

                    case "bordereauID":
                        query.append(nextConnective).append(" o.bordereauID LIKE :bordereauID");
                        queryParameters.put("bordereauID", "%" + filters.get(filterProperty) + "%");
                        break;

                    case "recuID":
                        query.append(nextConnective).append(" o.recuID LIKE :recuID");
                        queryParameters.put("recuID", "%" + filters.get(filterProperty) + "%");
                        break;

                    case "date":
                        query.append(nextConnective).append(" o.date = :date");
                        queryParameters.put("date", filters.get(filterProperty));
                        break;

                    case "nom":
                        query.append(nextConnective).append(" o.nom LIKE :nom");
                        queryParameters.put("nom", "%" + filters.get(filterProperty) + "%");
                        break;

                    case "postNom":
                        query.append(nextConnective).append(" o.postNom LIKE :postNom");
                        queryParameters.put("postNom", "%" + filters.get(filterProperty) + "%");
                        break;

                    case "prenom":
                        query.append(nextConnective).append(" o.prenom LIKE :prenom");
                        queryParameters.put("prenom", "%" + filters.get(filterProperty) + "%");
                        break;

                    case "adresse":
                        query.append(nextConnective).append(" o.adresse LIKE :adresse");
                        queryParameters.put("adresse", "%" + filters.get(filterProperty) + "%");
                        break;

                    case "dateNaissance":
                        query.append(nextConnective).append(" o.dateNaissance = :dateNaissance");
                        queryParameters.put("dateNaissance", filters.get(filterProperty));
                        break;

                    case "lieuNaissance":
                        query.append(nextConnective).append(" o.lieuNaissance LIKE :lieuNaissance");
                        queryParameters.put("lieuNaissance", "%" + filters.get(filterProperty) + "%");
                        break;

                    case "telephone":
                        query.append(nextConnective).append(" o.telephone LIKE :telephone");
                        queryParameters.put("telephone", "%" + filters.get(filterProperty) + "%");
                        break;

                    case "email":
                        query.append(nextConnective).append(" o.email LIKE :email");
                        queryParameters.put("email", "%" + filters.get(filterProperty) + "%");
                        break;

                    case "contratID":
                        query.append(nextConnective).append(" o.contratID LIKE :contratID");
                        queryParameters.put("contratID", "%" + filters.get(filterProperty) + "%");
                        break;

                    case "statut":
                        query.append(nextConnective).append(" o.statut = :statut");
                        queryParameters.put("statut", ContratStatut.valueOf(filters.get(filterProperty).toString()));
                        break;

                    case "contratNumber":
                        query.append(nextConnective).append(" o.contratNumber = :contratNumber");
                        queryParameters.put("contratNumber", new Integer(filters.get(filterProperty).toString()));
                        break;

                    case "dateEcheance1":
                        query.append(nextConnective).append(" o.dateEcheance1 = :dateEcheance1");
                        queryParameters.put("dateEcheance1", filters.get(filterProperty));
                        break;

                    case "dateEcheance2":
                        query.append(nextConnective).append(" o.dateEcheance2 = :dateEcheance2");
                        queryParameters.put("dateEcheance2", filters.get(filterProperty));
                        break;

                    case "dateEcheance3":
                        query.append(nextConnective).append(" o.dateEcheance3 = :dateEcheance3");
                        queryParameters.put("dateEcheance3", filters.get(filterProperty));
                        break;

                    case "echeance1Payed":
                        query.append(nextConnective).append(" o.echeance1Payed = :echeance1Payed");
                        queryParameters.put("echeance1Payed", filters.get(filterProperty));
                        break;

                    case "echeance2Payed":
                        query.append(nextConnective).append(" o.echeance2Payed = :echeance2Payed");
                        queryParameters.put("echeance2Payed", filters.get(filterProperty));
                        break;

                    case "echeance3Payed":
                        query.append(nextConnective).append(" o.echeance3Payed = :echeance3Payed");
                        queryParameters.put("echeance3Payed", filters.get(filterProperty));
                        break;

                    case "agence":
                        query.append(nextConnective).append(" o.agence = :agence");
                        queryParameters.put("agence", filters.get(filterProperty));
                        break;

                    case "user":
                        query.append(nextConnective).append(" o.user = :user");
                        queryParameters.put("user", filters.get(filterProperty));
                        break;

                    case "agence2":
                        query.append(nextConnective).append(" o.agence2 = :agence2");
                        queryParameters.put("agence2", filters.get(filterProperty));
                        break;

                }

                nextConnective = " AND";
            }

            query.append(" ) ");
            nextConnective = " AND";
        }

        if (sortField != null && !sortField.isEmpty()) {
            query.append(" ORDER BY o.").append(sortField);
            query.append(SortOrder.DESCENDING.equals(sortOrder) ? " DESC" : " ASC");
        }

        TypedQuery<ContratEntity> q = this.entityManager.createQuery(query.toString(), this.getType());

        for (String queryParameter : queryParameters.keySet()) {
            q.setParameter(queryParameter, queryParameters.get(queryParameter));
        }

        return q.setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }

    public long getMaxContractNumerForAgence(AgenceEntity agence) {
        try {
            return entityManager.createQuery("SELECT MAX (o.contratNumber) FROM Contrat o where o.agence = :agence", Long.class)
                    .setParameter("agence", agence).getSingleResult();
        } catch (Exception ex) {
            return 0L;
        }
    }

    public List<ContratEntity> allContractForToday() {
        ArrayList<ContratEntity> c = new ArrayList<>();
        c.addAll(allContractEcheance1ForToday());
        System.out.println("allContractEcheance1ForToday() = " + allContractEcheance1ForToday());
        c.addAll(allContractEcheance2ForToday());
        System.out.println("allContractEcheance2ForToday() = " + allContractEcheance2ForToday());
        c.addAll(allContractEcheance3ForToday());
        System.out.println("allContractEcheance3ForToday() = " + allContractEcheance3ForToday());
        return c;
    }

    public List<ContratEntity> allContractForThisMonth() {
        ArrayList<ContratEntity> c = new ArrayList<>();
        c.addAll(allContractEcheance1ThisMonth());
        System.out.println("allContractEcheance1ThisMonth() = " + allContractEcheance1ThisMonth());
        c.addAll(allContractEcheance2ThisMonth());
        System.out.println("allContractEcheance2ThisMonth() = " + allContractEcheance2ThisMonth());
        c.addAll(allContractEcheance3ThisMonth());
        System.out.println("allContractEcheance3ThisMonth() = " + allContractEcheance3ThisMonth());
        return c;
    }

    public List<ContratEntity> allContractEcheance1ForToday() {
        return entityManager.createQuery("SELECT o FROM Contrat o WHERE o.dateEcheance1 = current_date", ContratEntity.class)
//            .setParameter("date", new Date())
                .getResultList();
    }

    public List<ContratEntity> allContractEcheance2ForToday() {
        return entityManager.createQuery("SELECT o FROM Contrat o WHERE o.dateEcheance2 = current_date", ContratEntity.class)
                .getResultList();
    }

    public List<ContratEntity> allContractEcheance3ForToday() {
        return entityManager.createQuery("SELECT o FROM Contrat o WHERE o.dateEcheance3 = current_date", ContratEntity.class)
                .getResultList();
    }

    public List<ContratEntity> allContractEcheance1ThisMonth() {
        return entityManager.createQuery("SELECT o FROM Contrat o WHERE function('MONTH', o.dateEcheance1) = function('MONTH', current_date ) and function('YEAR', o.dateEcheance1) = function('YEAR', current_date )", ContratEntity.class)
                .getResultList();
    }

    public List<ContratEntity> allContractEcheance2ThisMonth() {
        return entityManager.createQuery("SELECT o FROM Contrat o WHERE function('MONTH', o.dateEcheance2) = function('MONTH', current_date ) and function('YEAR', o.dateEcheance2) = function('YEAR', current_date )", ContratEntity.class)
                .getResultList();
    }

    public List<ContratEntity> allContractEcheance3ThisMonth() {
        return entityManager.createQuery("SELECT o FROM Contrat o WHERE function('MONTH', o.dateEcheance3) = function('MONTH', current_date ) and function('YEAR', o.dateEcheance3) = function('YEAR', current_date )", ContratEntity.class)
                .getResultList();
    }

    @Transactional
    public BigDecimal sumContratsByMonth() {
        return entityManager.createQuery("SELECT SUM(o.montant) FROM Contrat o WHERE FUNCTION ('MONTH',o.date) = :month and FUNCTION ('YEAR', o.date)=:year", BigDecimal.class)
                .setParameter("month", (new Date().getMonth()) + 1)
                .setParameter("year", (new Date().getYear()) + 1900)
                .getSingleResult();
    }

    @Transactional
    public BigDecimal sumContratsByMonth(int month) {
        return entityManager.createQuery("SELECT SUM(o.montant) FROM Contrat o WHERE FUNCTION ('MONTH',o.date) = :month and FUNCTION ('YEAR', o.date)=:year", BigDecimal.class)
                .setParameter("month", (month) + 1)
                .setParameter("year", (new Date().getYear()) + 1900)
                .getSingleResult();
    }

    @Transactional
    public ContratEntity updateWithoutUser(ContratEntity contrat) {
        return super.update(contrat);
    }




}
