package cd.forexy.service;

import cd.forexy.domain.*;

import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Stateless
public class ContractTimer {

    @Inject
    private ContratService contratService;
    @Inject
    private RetraitService retraitService;

    @Schedule(dayOfWeek = "*", hour = "*", minute = "*")
//    @Schedule(hour = "1", dayOfWeek = "*")
    public void createPayments() {

        List<ContratEntity> allContractEcheance1 = contratService.allContractEcheance1ForToday();
        for (ContratEntity contratEntity : allContractEcheance1) {
            if (!contratEntity.getEcheance1Payed()) {
                createPayment(contratEntity, RetraitEcheance.UN, 1);
            }
        }

        List<ContratEntity> allContractEcheance2 = contratService.allContractEcheance2ForToday();

        for (ContratEntity contratEntity : allContractEcheance2) {
            if (!contratEntity.getEcheance2Payed()) {
                createPayment(contratEntity, RetraitEcheance.DEUX, 1);
            }
        }
        List<ContratEntity> allContractEcheance3 = contratService.allContractEcheance3ForToday();

        for (ContratEntity contratEntity : allContractEcheance3) {
            if (!contratEntity.getEcheance3Payed()) {
                int amountMultiplier = 2;
                createPayment(contratEntity, RetraitEcheance.TROIS, amountMultiplier);
            }
        }
    }

    public void createPayment(ContratEntity contratEntity, RetraitEcheance echeance, int amountMultiplier) {
        RetraitEntity retraitEntity = new RetraitEntity();
        switch (echeance) {
            case UN:
                contratEntity.setEcheance1Payed(true);
                break;
            case DEUX:
                contratEntity.setEcheance2Payed(true);
                break;
            case TROIS:
                contratEntity.setEcheance3Payed(true);
                // desactiver le contrat apres 3 echeances
                contratEntity.setStatut(ContratStatut.INACTIF);
                break;
        }
        contratService.updateWithoutUser(contratEntity);

        retraitEntity.setDate(new Date());
        retraitEntity.setContrat(contratEntity);
        retraitEntity.setAgence(contratEntity.getAgence());
        retraitEntity.setEcheance(echeance);
        retraitEntity.setStatus(RetraitStatus.INITIE);
        retraitEntity.setMontant(contratEntity.getMontant().multiply(BigDecimal.valueOf(amountMultiplier)));
        retraitService.saveWithoutUser(retraitEntity);
    }
}
