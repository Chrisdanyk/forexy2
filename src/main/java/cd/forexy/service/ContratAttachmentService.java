package cd.forexy.service;

import cd.forexy.domain.ContratAttachment;
import cd.forexy.domain.ContratEntity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;
import javax.transaction.Transactional;

@Named
public class ContratAttachmentService extends BaseService<ContratAttachment> implements Serializable {

    private static final long serialVersionUID = 1L;
    
    public ContratAttachmentService(){
        super(ContratAttachment.class);
    }
    
    @Override
    @Transactional
    public long countAllEntries() {
        return entityManager.createQuery("SELECT COUNT(o) FROM ContratAttachment o", Long.class).getSingleResult();
    }

    @Transactional
    public void deleteAttachmentsByContrat(ContratEntity contrat) {
        entityManager
                .createQuery("DELETE FROM ContratAttachment c WHERE c.contrat = :p")
                .setParameter("p", contrat).executeUpdate();
    }
    
    @Transactional
    public List<ContratAttachment> getAttachmentsList(ContratEntity contrat) {
        if (contrat == null || contrat.getId() == null) {
            return new ArrayList<>();
        }
        // The byte streams are not loaded from database with following line. This would cost too much.
        return entityManager.createQuery("SELECT NEW cd.forexy.domain.ContratAttachment(o.id, o.fileName) FROM ContratAttachment o WHERE o.contrat.id = :id", ContratAttachment.class).setParameter("id", contrat.getId()).getResultList();
    }
}
