package cd.forexy.service.security;

import cd.forexy.domain.security.RolePermission;
import cd.forexy.domain.security.UserRole;

import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

@Singleton
@Startup
public class RolePermissionsPublisher {

    private static final Logger logger = Logger.getLogger(RolePermissionsPublisher.class.getName());
    
    @Inject
    private RolePermissionsService rolePermissionService;
    
    @PostConstruct
    public void postConstruct() {

        if (rolePermissionService.countAllEntries() == 0) {

            rolePermissionService.save(new RolePermission(UserRole.Administrator, "userDetails:create"));
            
            rolePermissionService.save(new RolePermission(UserRole.Administrator, "userDetails:read"));
            
            rolePermissionService.save(new RolePermission(UserRole.Administrator, "userDetails:update"));
            
            rolePermissionService.save(new RolePermission(UserRole.Administrator, "userDetails:delete"));
            
            rolePermissionService.save(new RolePermission(UserRole.Daf, "userDetails:create"));
            
            rolePermissionService.save(new RolePermission(UserRole.Daf, "userDetails:read"));
            
            rolePermissionService.save(new RolePermission(UserRole.Daf, "userDetails:update"));
            
            rolePermissionService.save(new RolePermission(UserRole.Daf, "userDetails:delete"));
            
            rolePermissionService.save(new RolePermission(UserRole.Dg, "userDetails:create"));
            
            rolePermissionService.save(new RolePermission(UserRole.Dg, "userDetails:read"));
            
            rolePermissionService.save(new RolePermission(UserRole.Dg, "userDetails:update"));
            
            rolePermissionService.save(new RolePermission(UserRole.Dg, "userDetails:delete"));
            
            rolePermissionService.save(new RolePermission(UserRole.Agent, "userDetails:create"));
            
            rolePermissionService.save(new RolePermission(UserRole.Agent, "userDetails:read"));
            
            rolePermissionService.save(new RolePermission(UserRole.Agent, "userDetails:update"));
            
            rolePermissionService.save(new RolePermission(UserRole.Agent, "userDetails:delete"));
            
            rolePermissionService.save(new RolePermission(UserRole.Administrator, "mouvement:create"));
            
            rolePermissionService.save(new RolePermission(UserRole.Administrator, "mouvement:read"));
            
            rolePermissionService.save(new RolePermission(UserRole.Administrator, "mouvement:update"));
            
            rolePermissionService.save(new RolePermission(UserRole.Daf, "mouvement:read"));
            
            rolePermissionService.save(new RolePermission(UserRole.Dg, "mouvement:read"));
            
            rolePermissionService.save(new RolePermission(UserRole.AgentReception, "mouvement:create"));
            
            rolePermissionService.save(new RolePermission(UserRole.AgentReception, "mouvement:read"));
            
            rolePermissionService.save(new RolePermission(UserRole.AgentReception, "mouvement:update"));
            
            rolePermissionService.save(new RolePermission(UserRole.Administrator, "agence:create"));
            
            rolePermissionService.save(new RolePermission(UserRole.Administrator, "agence:read"));
            
            rolePermissionService.save(new RolePermission(UserRole.Administrator, "agence:update"));
            
            rolePermissionService.save(new RolePermission(UserRole.Daf, "agence:create"));
            
            rolePermissionService.save(new RolePermission(UserRole.Daf, "agence:read"));
            
            rolePermissionService.save(new RolePermission(UserRole.Daf, "agence:update"));
            
            rolePermissionService.save(new RolePermission(UserRole.Dg, "agence:create"));
            
            rolePermissionService.save(new RolePermission(UserRole.Dg, "agence:read"));
            
            rolePermissionService.save(new RolePermission(UserRole.Dg, "agence:update"));
            
            rolePermissionService.save(new RolePermission(UserRole.Agent, "agence:read"));
            
            rolePermissionService.save(new RolePermission(UserRole.Administrator, "contrat:create"));
            
            rolePermissionService.save(new RolePermission(UserRole.Administrator, "contrat:read"));
            
            rolePermissionService.save(new RolePermission(UserRole.Administrator, "contrat:update"));
            
            rolePermissionService.save(new RolePermission(UserRole.Daf, "contrat:create"));
            
            rolePermissionService.save(new RolePermission(UserRole.Daf, "contrat:read"));
            
            rolePermissionService.save(new RolePermission(UserRole.Daf, "contrat:update"));
            
            rolePermissionService.save(new RolePermission(UserRole.Dg, "contrat:create"));
            
            rolePermissionService.save(new RolePermission(UserRole.Dg, "contrat:read"));
            
            rolePermissionService.save(new RolePermission(UserRole.Dg, "contrat:update"));
            
            rolePermissionService.save(new RolePermission(UserRole.Agent, "contrat:create"));
            
            rolePermissionService.save(new RolePermission(UserRole.Agent, "contrat:read"));
            
            rolePermissionService.save(new RolePermission(UserRole.Agent, "contrat:update"));
            
            rolePermissionService.save(new RolePermission(UserRole.Administrator, "retrait:create"));
            
            rolePermissionService.save(new RolePermission(UserRole.Administrator, "retrait:read"));
            
            rolePermissionService.save(new RolePermission(UserRole.Administrator, "retrait:update"));
            
            rolePermissionService.save(new RolePermission(UserRole.Daf, "retrait:create"));
            
            rolePermissionService.save(new RolePermission(UserRole.Daf, "retrait:read"));
            
            rolePermissionService.save(new RolePermission(UserRole.Daf, "retrait:update"));
            
            rolePermissionService.save(new RolePermission(UserRole.Dg, "retrait:create"));
            
            rolePermissionService.save(new RolePermission(UserRole.Dg, "retrait:read"));
            
            rolePermissionService.save(new RolePermission(UserRole.Dg, "retrait:update"));
            
            rolePermissionService.save(new RolePermission(UserRole.Administrator, "parametre:create"));
            
            rolePermissionService.save(new RolePermission(UserRole.Administrator, "parametre:read"));
            
            rolePermissionService.save(new RolePermission(UserRole.Administrator, "parametre:update"));
            
            rolePermissionService.save(new RolePermission(UserRole.Daf, "parametre:create"));
            
            rolePermissionService.save(new RolePermission(UserRole.Daf, "parametre:read"));
            
            rolePermissionService.save(new RolePermission(UserRole.Daf, "parametre:update"));
            
            rolePermissionService.save(new RolePermission(UserRole.Dg, "parametre:create"));
            
            rolePermissionService.save(new RolePermission(UserRole.Dg, "parametre:read"));
            
            rolePermissionService.save(new RolePermission(UserRole.Dg, "parametre:update"));
            
            rolePermissionService.save(new RolePermission(UserRole.Agent, "parametre:read"));
            
            rolePermissionService.save(new RolePermission(UserRole.Administrator, "user:*"));
            
            logger.info("Successfully created permissions for user roles.");
        }
    }
}
