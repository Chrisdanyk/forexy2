package cd.forexy.service.security;

import cd.forexy.domain.security.UserEntity;
import cd.forexy.domain.security.UserRole;
import cd.forexy.domain.security.UserStatus;

import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

/**
 * Creates some test users in fresh database.
 * 
 * TODO This class is temporary for test, only. Just delete this class
 * if you do not need the test users to be created automatically.
 *
 */
@Singleton
@Startup
public class TestUsersCreator {

    private static final Logger logger = Logger.getLogger(TestUsersCreator.class.getName());
    
    @Inject
    private UserService userService;
    
    @PostConstruct
    public void postConstruct() {
        
       if(userService.countAllEntries() == 0) {
           
            logger.log(Level.WARNING, "Creating test user 'admin' with password 'admin'.");
            UserEntity admin = new UserEntity();
            admin.setUsername("admin");
            admin.setPassword("admin");
            admin.setRoles(Arrays.asList(new UserRole[]{UserRole.Administrator}));
            admin.setStatus(UserStatus.Active);
            admin.setEmail("admin@domain.test");
            
            userService.save(admin);
            
            logger.log(Level.WARNING, "Creating test user 'daf' with password 'daf'.");
            UserEntity dafUser = new UserEntity();
            dafUser.setUsername("daf");
            dafUser.setPassword("daf");
            dafUser.setRoles(Arrays.asList(new UserRole[]{UserRole.Daf}));
            dafUser.setStatus(UserStatus.Active);
            dafUser.setEmail("daf@domain.test");
            
            userService.save(dafUser);
            
            logger.log(Level.WARNING, "Creating test user 'dg' with password 'dg'.");
            UserEntity dgUser = new UserEntity();
            dgUser.setUsername("dg");
            dgUser.setPassword("dg");
            dgUser.setRoles(Arrays.asList(new UserRole[]{UserRole.Dg}));
            dgUser.setStatus(UserStatus.Active);
            dgUser.setEmail("dg@domain.test");
            
            userService.save(dgUser);
            
            logger.log(Level.WARNING, "Creating test user 'agent' with password 'agent'.");
            UserEntity agentUser = new UserEntity();
            agentUser.setUsername("agent");
            agentUser.setPassword("agent");
            agentUser.setRoles(Arrays.asList(new UserRole[]{UserRole.Agent}));
            agentUser.setStatus(UserStatus.Active);
            agentUser.setEmail("agent@domain.test");
            
            userService.save(agentUser);
            
            logger.log(Level.WARNING, "Creating test user 'agentReception' with password 'agentReception'.");
            UserEntity agentReceptionUser = new UserEntity();
            agentReceptionUser.setUsername("agentReception");
            agentReceptionUser.setPassword("agentReception");
            agentReceptionUser.setRoles(Arrays.asList(new UserRole[]{UserRole.AgentReception}));
            agentReceptionUser.setStatus(UserStatus.Active);
            agentReceptionUser.setEmail("agentReception@domain.test");
            
            userService.save(agentReceptionUser);
            
            logger.log(Level.WARNING, "Creating test user 'registered' with password 'registered'.");
            UserEntity registeredUser = new UserEntity();
            registeredUser.setUsername("registered");
            registeredUser.setPassword("registered");
            registeredUser.setRoles(Arrays.asList(new UserRole[]{UserRole.Registered}));
            registeredUser.setStatus(UserStatus.Active);
            registeredUser.setEmail("registered@domain.test");
            
            userService.save(registeredUser);
            
        }
    }
}
