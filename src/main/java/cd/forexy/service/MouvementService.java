package cd.forexy.service;

import cd.forexy.domain.AgenceEntity;
import cd.forexy.domain.MouvementEntity;
import cd.forexy.domain.security.UserEntity;

import java.io.Serializable;
import java.util.List;

import javax.inject.Named;
import javax.transaction.Transactional;

@Named
public class MouvementService extends BaseService<MouvementEntity> implements Serializable {

    private static final long serialVersionUID = 1L;
    
    public MouvementService(){
        super(MouvementEntity.class);
    }
    
    @Transactional
    public List<MouvementEntity> findAllMouvementEntities() {
        
        return entityManager.createQuery("SELECT o FROM Mouvement o ", MouvementEntity.class).getResultList();
    }
    
    @Override
    @Transactional
    public long countAllEntries() {
        return entityManager.createQuery("SELECT COUNT(o) FROM Mouvement o", Long.class).getSingleResult();
    }
    
    @Override
    protected void handleDependenciesBeforeDelete(MouvementEntity mouvement) {

        /* This is called before a Mouvement is deleted. Place here all the
           steps to cut dependencies to other entities */
        
    }

    @Transactional
    public List<MouvementEntity> findAvailableMouvements(UserEntity user) {
        return entityManager.createQuery("SELECT o FROM Mouvement o WHERE o.user IS NULL", MouvementEntity.class).getResultList();
    }

    @Transactional
    public List<MouvementEntity> findMouvementsByUser(UserEntity user) {
        return entityManager.createQuery("SELECT o FROM Mouvement o WHERE o.user = :user", MouvementEntity.class).setParameter("user", user).getResultList();
    }

    @Transactional
    public List<MouvementEntity> findAvailableMouvements(AgenceEntity agence) {
        return entityManager.createQuery("SELECT o FROM Mouvement o WHERE o.agence IS NULL", MouvementEntity.class).getResultList();
    }

    @Transactional
    public List<MouvementEntity> findMouvementsByAgence(AgenceEntity agence) {
        return entityManager.createQuery("SELECT o FROM Mouvement o WHERE o.agence = :agence", MouvementEntity.class).setParameter("agence", agence).getResultList();
    }

}
