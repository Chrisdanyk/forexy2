package cd.forexy.service;

import cd.forexy.domain.AgenceEntity;
import cd.forexy.domain.ContratEntity;
import cd.forexy.domain.RetraitEcheance;
import cd.forexy.domain.RetraitEntity;
import cd.forexy.domain.RetraitStatus;
import cd.forexy.domain.security.UserEntity;
import cd.forexy.service.security.SecurityWrapper;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Named;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.primefaces.model.SortOrder;

@Named
public class RetraitService extends BaseService<RetraitEntity> implements Serializable {

    private static final long serialVersionUID = 1L;

    public RetraitService() {
        super(RetraitEntity.class);
    }

    @Transactional
    public List<RetraitEntity> findAllRetraitEntities() {

        return entityManager.createQuery("SELECT o FROM Retrait o ", RetraitEntity.class).getResultList();
    }


    @Override
    @Transactional
    public RetraitEntity save(RetraitEntity retrait) {
        String username = SecurityWrapper.getUsername();

        retrait.updateAuditInformation(username);

        return super.save(retrait);
    }

    @Override
    @Transactional
    public RetraitEntity update(RetraitEntity retrait) {
        String username = SecurityWrapper.getUsername();
        retrait.updateAuditInformation(username);
        return super.update(retrait);
    }

    @Override
    protected void handleDependenciesBeforeDelete(RetraitEntity retrait) {

        /* This is called before a Retrait is deleted. Place here all the
           steps to cut dependencies to other entities */

    }

    @Transactional
    public List<RetraitEntity> findAvailableRetraits(ContratEntity contrat) {
        return entityManager.createQuery("SELECT o FROM Retrait o WHERE o.contrat IS NULL", RetraitEntity.class).getResultList();
    }

    @Transactional
    public List<RetraitEntity> findRetraitsByContrat(ContratEntity contrat) {
        return entityManager.createQuery("SELECT o FROM Retrait o WHERE o.contrat = :contrat", RetraitEntity.class).setParameter("contrat", contrat).getResultList();
    }

    @Transactional
    public List<RetraitEntity> findAvailableRetraits(UserEntity user) {
        return entityManager.createQuery("SELECT o FROM Retrait o WHERE o.user IS NULL", RetraitEntity.class).getResultList();
    }

    @Transactional
    public List<RetraitEntity> findRetraitsByUser(UserEntity user) {
        return entityManager.createQuery("SELECT o FROM Retrait o WHERE o.user = :user", RetraitEntity.class).setParameter("user", user).getResultList();
    }

    @Transactional
    public List<RetraitEntity> findAvailableRetraits(AgenceEntity agence) {
        return entityManager.createQuery("SELECT o FROM Retrait o WHERE o.agence IS NULL", RetraitEntity.class).getResultList();
    }

    @Transactional
    public List<RetraitEntity> findRetraitsByAgence(AgenceEntity agence) {
        return entityManager.createQuery("SELECT o FROM Retrait o WHERE o.agence = :agence", RetraitEntity.class).setParameter("agence", agence).getResultList();
    }

    // This is the central method called by the DataTable
    @Override
    @Transactional
    public List<RetraitEntity> findEntriesPagedAndFilteredAndSorted(int firstResult, int maxResults, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

        StringBuilder query = new StringBuilder();

        query.append("SELECT o FROM Retrait o");

        // Can be optimized: We need this join only when contrat filter is set
        query.append(" LEFT OUTER JOIN o.contrat contrat");

        // Can be optimized: We need this join only when user filter is set
        query.append(" LEFT OUTER JOIN o.user user");

        // Can be optimized: We need this join only when agence filter is set
        query.append(" LEFT OUTER JOIN o.agence agence");

        String nextConnective = " WHERE";

        Map<String, Object> queryParameters = new HashMap<>();

        if (filters != null && !filters.isEmpty()) {

            nextConnective += " ( ";

            for (String filterProperty : filters.keySet()) {

                if (filters.get(filterProperty) == null) {
                    continue;
                }

                switch (filterProperty) {

                    case "montant":
                        query.append(nextConnective).append(" o.montant = :montant");
                        queryParameters.put("montant", new BigDecimal(filters.get(filterProperty).toString()));
                        break;

                    case "date":
                        query.append(nextConnective).append(" o.date = :date");
                        queryParameters.put("date", filters.get(filterProperty));
                        break;

                    case "status":
                        query.append(nextConnective).append(" o.status = :status");
                        queryParameters.put("status", RetraitStatus.valueOf(filters.get(filterProperty).toString()));
                        break;

                    case "echeance":
                        query.append(nextConnective).append(" o.echeance = :echeance");
                        queryParameters.put("echeance", RetraitEcheance.valueOf(filters.get(filterProperty).toString()));
                        break;

                    case "contrat":
                        query.append(nextConnective).append(" o.contrat = :contrat");
                        queryParameters.put("contrat", filters.get(filterProperty));
                        break;

                    case "user":
                        query.append(nextConnective).append(" o.user = :user");
                        queryParameters.put("user", filters.get(filterProperty));
                        break;

                    case "agence":
                        query.append(nextConnective).append(" o.agence = :agence");
                        queryParameters.put("agence", filters.get(filterProperty));
                        break;

                }

                nextConnective = " AND";
            }

            query.append(" ) ");
            nextConnective = " AND";
        }

        if (sortField != null && !sortField.isEmpty()) {
            query.append(" ORDER BY o.").append(sortField);
            query.append(SortOrder.DESCENDING.equals(sortOrder) ? " DESC" : " ASC");
        }

        TypedQuery<RetraitEntity> q = this.entityManager.createQuery(query.toString(), this.getType());

        for (String queryParameter : queryParameters.keySet()) {
            q.setParameter(queryParameter, queryParameters.get(queryParameter));
        }

        return q.setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }

    public List<RetraitEntity> allRetraitsThisMonthByAgence(AgenceEntity agence) {
        return entityManager.createQuery("SELECT o FROM Retrait o WHERE o.agence=:agence and function('MONTH', o.date) = function('MONTH', current_date ) and function('YEAR', o.date) = function('YEAR', current_date ) and o.status = :status", RetraitEntity.class)
                .setParameter("agence", agence)
                .setParameter("status", RetraitStatus.INITIE)
                .getResultList();
    }

    public Long countallRetraitsThisMonthByAgence(AgenceEntity agence) {
        return entityManager.createQuery("SELECT COUNT(o) FROM Retrait o WHERE o.agence=:agence and function('MONTH', o.date) = function('MONTH', current_date ) and function('YEAR', o.date) = function('YEAR', current_date ) and o.status = :status", Long.class)
                .setParameter("agence", agence)
                .setParameter("status", RetraitStatus.INITIE)
                .getSingleResult();
    }

    public List<RetraitEntity> allRetraitsTodayByAgence(AgenceEntity agence) {
        return entityManager.createQuery("SELECT o FROM Retrait o WHERE o.agence=:agence and o.date = current_date and o.status = :status", RetraitEntity.class)
                .setParameter("agence", agence)
                .setParameter("status", RetraitStatus.INITIE)
                .getResultList();
    }

    public Long countallRetraitsTodayByAgence(AgenceEntity agence) {
        return entityManager.createQuery("SELECT COUNT(o) FROM Retrait o WHERE o.agence=:agence and o.date = current_date and o.status = :status", Long.class)
                .setParameter("agence", agence)
                .setParameter("status", RetraitStatus.INITIE)
                .getSingleResult();
    }

    public List<RetraitEntity> allRetraitsToday() {
        return entityManager.createQuery("SELECT o FROM Retrait o WHERE  o.date = current_date", RetraitEntity.class)
                .getResultList();
    }

    public List<ContratEntity> allPaimentThisMonth() {
        return entityManager.createQuery("SELECT o FROM Contrat o WHERE function('MONTH', o.dateEcheance3) = function('MONTH', current_date ) and function('YEAR', o.dateEcheance3) = function('YEAR', current_date )", ContratEntity.class)
                .getResultList();
    }


    public List<RetraitEntity> allRetraitsByAgence(AgenceEntity agence) {
        return entityManager.createQuery("SELECT o FROM Retrait o WHERE o.agence=:agence", RetraitEntity.class)
                .setParameter("agence", agence)
                .getResultList();
    }

    @Override
    @Transactional
    public long countAllEntries() {
        return entityManager.createQuery("SELECT COUNT(o) FROM Retrait o WHERE o.status=cd.forexy.domain.RetraitStatus.CONFIRME", Long.class).getSingleResult();
    }

    @Transactional
    public BigDecimal sumRetraitsByMonth() {
        return entityManager.createQuery("SELECT SUM(o.montant) FROM Retrait o WHERE FUNCTION ('MONTH',o.date) = :month and FUNCTION ('YEAR', o.date)=:year and o.status=cd.forexy.domain.RetraitStatus.CONFIRME", BigDecimal.class)
                .setParameter("month", (new Date().getMonth()) + 1)
                .setParameter("year", (new Date().getYear()) + 1900)
                .getSingleResult();
    }

    @Transactional
    public BigDecimal sumRetraitsByMonth(int month) {
        return entityManager.createQuery("SELECT SUM(o.montant) FROM Retrait o WHERE FUNCTION ('MONTH',o.date) = :month and FUNCTION ('YEAR', o.date)=:year and o.status=cd.forexy.domain.RetraitStatus.CONFIRME", BigDecimal.class)
                .setParameter("month", (month) + 1)
                .setParameter("year", (new Date().getYear()) + 1900)
                .getSingleResult();
    }

    @Transactional
    public RetraitEntity saveWithoutUser(RetraitEntity retrait) {
        return super.save(retrait);
    }

}
