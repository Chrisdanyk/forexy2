package cd.forexy.print;


import com.itextpdf.text.*;
import com.itextpdf.text.Font;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.qrcode.EncodeHintType;
import com.itextpdf.text.pdf.qrcode.ErrorCorrectionLevel;
import org.json.JSONObject;

import java.awt.*;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;


public class TestMain {

    public static void main(String[] args) {

        try {


            String path = "/home/joe/Desktop/tetdocu3.pdf";

            // create document
            Document document = new Document(PageSize.A4, 36, 36, 90, 50);
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(path));

            // add header and footer
            HeaderFooterPageEvent event = new HeaderFooterPageEvent();
            writer.setPageEvent(event);

            // write to document
            document.open();
            buildDocument(document);

            document.close();

//            String xx = "6884ffab-b972-4d8b-ae09-55354bd423be-54365";
//
//            System.out.println("xx = " + xx.length());
//
//            String substring = xx.substring(37);
//
//            System.out.println("substring = " + substring);
//
////            ResourceBundle bundle = ResourceBundle.getBundle("settings");
////
////            String imagesPath = ResourceBundle.getBundle("settings").getString("images.path");
//            String imagesPath = "/home/joe/ticketing";
////            String jasperFilePath = ResourceBundle.getBundle("settings").getString("jasper.path");
//
//            System.out.println("imagesPath = " + imagesPath);
//
//            Rectangle envelope = new Rectangle(500, 160);
//
//            String path = imagesPath + "/Forexy-" + UUID.randomUUID() + ".pdf";
////            Document document = new Document(PageSize.A6);
////            Document document = new Document(envelope, 5, 2, 4, 2);
//            Document document = new Document(PageSize.A4, 20, 20, 20, 20);
//
//            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(path));
////            writer.setPageEvent(new PDFEventListener());
//
//            document.open();
//
//            addOrderToDocument(document);
//
//            document.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    static void buildDocument(Document document) throws DocumentException, IOException {
        Font regular = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);
        Font bold = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);

        Phrase p;
        p = new Phrase("Contrat d’investissement\n", bold);
        p.add(new Chunk("N°0022/INV/FXG-GM/2021\n\n", bold));
        Paragraph paragraph = new Paragraph();
        paragraph.setAlignment(Element.ALIGN_CENTER);
        paragraph.add(p);
        document.add(paragraph);

        paragraph = new Paragraph();
        paragraph.setAlignment(Element.ALIGN_JUSTIFIED);
        p = new Phrase("Conclu entre\n\n", regular);
        p.add(new Chunk("FOREXY GROUP Sarl, ", bold));
        p.add(new Chunk("immatriculé au registre de commerce et du crédit mobilier de " +
            "Lubumbashi sous le numéro ", regular));
        p.add(new Chunk("CD/LSH/RCCM/21-B-00017, idnat 05-H5300-N71610W", bold));
        p.add(new Chunk(" et ayant son siege au 20 av Champ du potier, C/ Lubumbashi, V/Lubumbashi, P/Haut-Katanga, " +
            "Représenté aux fins des présentes par ", regular));
        p.add(new Chunk("Monsieur ", bold));
        p.add(new Chunk("LUKEKA ", regular));
        p.add(new Chunk("MWANGA Hugues, ", bold));
        p.add(new Chunk("Directeur Général, et WAMU  ", regular));
        p.add(new Chunk("TYANZA Phibert Furet, ", bold));
        p.add(new Chunk("Directeur Administratif et Financier, ci-aprés dénommés les mandataires.\n\n", regular));

        p.add(new Chunk("Et\n", regular));
        p.add(new Chunk("Monsieur / Madame : ", regular));
        p.add(new Chunk("KAFIRONGO Benjamin\n\n", bold));
        p.add(new Chunk("Résident au : ", regular));
        p.add(new Chunk("Nord-Kivu\n\n", bold));
        p.add(new Chunk("République Démocratique du Congo, ci-aprés dénommeé le mandant.\n\n", regular));
        p.add(new Chunk("Piéce d'identité présentée :\n", regular));
        p.add(new Chunk("   Passeport [x]\n", regular));
        p.add(new Chunk("   Permis de conduire\n", regular));
        p.add(new Chunk("   Carte nationale d’électeur\n", regular));
        p.add(new Chunk("Numéro de téléphone : +243972738686\n\n", regular));
        p.add(new Chunk("Montant investi : ", regular));
        p.add(new Chunk("400$ ", bold));
        p.add(new Chunk(   "en toute lettre ", regular));
        p.add(new Chunk(   " quatre cent dollars americains\n\n ", bold));
//
        p.add(new Chunk("Préambule\n\n", bold));
        p.add(new Chunk("Les présentes clauses contractuelles servent de base a l'accord conclu entre le Mandant et le Mandataire," +
            " pour créer une collaboration confiante et constructive de part et d’autre.\n\n", regular));

        p.add(new Chunk("I. Droits et obligations découlant du Contrat\n\n", bold));
        p.add(new Chunk("Les conventions définies par écrit sont seules applicables au contrat conclu entre Le Mandant et le Mandataire. \n\n", regular));
        p.add(new Chunk("       Les modifications, accords ou promesses doivent etre confirmés par écrit. \n\n", regular));
//
        p.add(new Chunk("II. Mandat de gestion\n\n", bold));
        p.add(new Chunk("Le Mandant charge le Mandataire de la gestion de son capital investi, déposé auprés " +
            "d'une Banque choisie par le Mandataire. II n'est pas nécessaire que le Mandant donne son accord spécial pour " +
            "les opérations commerciales.\n\n", regular));
//
        p.add(new Chunk("III. Entendue du mandat\n\n", bold));
        p.add(new Chunk("Le Mandataire est seul et exclusivement habilité, par le pouvoir conféré par le Mandat à acquérir des devises et autres instruments financiers,  à son gré," +
            "  à vendre tous les titres et devises dont la vente lui paraitra opportuns, et en réinvestir le produit  à son gré.\n" +
            "Le Mandataire n'est pas habilité à effectuer des retraits ou à disposer autrement du compte du Mandant pour son propre compte ou en faveur de tiers.\n\n", regular));

        p.add(new Chunk("IV. Dénonciation et durée du Contrat\n\n", bold));
        p.add(new Chunk("Le présent contrat entre en vigueur au moment du versement de la somme a investir sur le compte désigné par" +
            " le Mandataire ou au guichet de FOREXY GROUP Sarl contre un bordereau de versement indiquant le montant et portant le numéro du présent contrat.\n\n" +
            "Il est conclu pour une durée ", regular));
        p.add(new Chunk("d’une année ", bold));
        p.add(new Chunk("et doit étre renouvelé par la signature d'un nouveau contrat.\n\n", regular));
        p.add(new Chunk("Le Contrat peut étre dénoncé à tout moment à la fin de l'échéance d'", regular));
        p.add(new Chunk("une année (12 mois) ", bold));
        p.add(new Chunk("a la demande de l'une des parties.\n\n", regular));
        p.add(new Chunk("De ce fait une pénalité de ", regular));
        p.add(new Chunk(" 35% ", bold));
        p.add(new Chunk("sera débitée au capital initial (capital investi) pour ceux qui voudront récupérer leur argent avant l’échéance d’", regular));
        p.add(new Chunk("une année (12 mois).\n\n", bold));
        p.add(new Chunk("Le présent mandat est maintenu sous réserve de circonstances contraignantes telles que décés, " +
            "déclarations de disparition, ou incapacité ou faillite du Mandataire.\n\n", regular));

        p.add(new Chunk("En cas d'empéchement, le mandat peut déléguer une personne moyennant une procuration.\n\n", regular));
        p.add(new Chunk("V. Rémunération\n\n", bold));
        p.add(new Chunk("Le Mandataire garanti au Mandant une performance de ", regular));
        p.add(new Chunk("25% ", bold));
        p.add(new Chunk("de gain par mois, soit ", regular));
        p.add(new Chunk("100% ", bold));
        p.add(new Chunk("de bénéfice chaque ", regular));
        p.add(new Chunk("quatre mois ", bold));
        p.add(new Chunk("du capital total investi.\nLe capital investit est maintenu pour une durée d’", regular));
        p.add(new Chunk("une année (12 mois).\n\n", bold));
        p.add(new Chunk("Le bénéfice est débité directement chaque fin de l'échéance de quatre mois selon le mode" +
            " de payement choisi par le Mandant. Les éventuelles rétrocessions et autres rémunérations des tiers reviennent au Mandataire.\n", bold));
        p.add(new Chunk("Les bénéfices générés ne sont en aucune circonstance automatiquement réinvestis.\n\n", bold));

        p.add(new Chunk("VI. Divers\n\n", bold));
        p.add(new Chunk("Aucun accord oral n'est conclu en dehors du présent contrat. Tout complement ou modification du contrat," +
            " ou toute convention concernant son annulation exige pour etre valable la forme écrite.\n\n", bold));

        p.add(new Chunk("Si l'une quelconque des dispositions du présent contrat s'avére par la suite invalide" +
            " pour un motif Juridique, les autres dispositions n'en seront pas affectées.\n" +
            "\n" +
            "Au lieu et Place de la disposition invalide, sera considérée comme convenue " +
            "toute disposition valide se rapprochant le plus de lesprit et du but de la disposition invalide, décidée " +
            "de bonne foi, conformément aux bonnes pratiques commerciales et aux us et coutumes habituels dans les opérations de cette " +
            "nature.\n" +
            "\n" +
            "La méme disposition s'applique à une lacune éventuelle du contrat. Tous les droits découlant de ce contrat " +
            "expirent en cas de résiliation légale par l’une des parties.\n\n", regular));

        p.add(new Chunk("VII. Connaissance des risques\n\n", bold));
        p.add(new Chunk("Le mandant confirme par sa signature qu'il a dûment été informé complétement et " +
            "suffisamment des risques de pertes et des possibilités de gains liés au commerce des devises et autres" +
            " placements possibles prévus au paragraphe III, et du fait que le Mandataire assume toute " +
            "responsabilité à cet égard.\n\n", regular));
        p.add(new Chunk("VIII. Des différends\n\n", bold));
        p.add(new Chunk("Tout différend qui pourrait résulter de I'interprétation ou de l'exécution du présent" +
            " contrat est réglé à l'amiable, en cas de désaccord persistant le litige sera soumis au tribunal compétent selon" +
            " les lois en vigueur en République Démocratique du Congo.\n\n", regular));

        p.add(new Chunk("Pour le compte de FOREXY GROUP\n\n", regular));

        paragraph.add(p);
        document.add(paragraph);


        float[] columnWidths = {5f, 5f};
        PdfPTable bossNamesTable = new PdfPTable(2);

        bossNamesTable.setTotalWidth(527);
        bossNamesTable.setLockedWidth(true);
        bossNamesTable.setWidthPercentage(100);
        bossNamesTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);
        PdfPCell cell;
        cell= new PdfPCell(new Phrase("LUKEKA MWANGA Hugues", bold));
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell.setBorder(Rectangle.NO_BORDER);
        bossNamesTable.addCell(cell);

        cell= new PdfPCell(new Phrase("WAMU IYANZA Phibert Furet", bold));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        bossNamesTable.addCell(cell);

        cell= new PdfPCell(new Phrase("Directeur Général", bold));
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell.setBorder(Rectangle.NO_BORDER);
        bossNamesTable.addCell(cell);

        cell= new PdfPCell(new Phrase("Directeur Administratif et Financier", regular));
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        bossNamesTable.addCell(cell);

        document.add(bossNamesTable);


        paragraph = new Paragraph();
        paragraph.setAlignment(Element.ALIGN_LEFT);
        p = new Phrase("\n\n\n\n\nSignature du Mandant\n\n\n\n\n", regular);
        paragraph.add(p);
        document.add(paragraph);

        paragraph = new Paragraph();
        paragraph.setAlignment(Element.ALIGN_RIGHT);
        p = new Phrase("Fait à Goma, le 10/04/2020", regular);
        paragraph.add(p);
        document.add(paragraph);


    }

    static Paragraph getParagraph(String text, int alignment) {
        Font bold = new Font(Font.FontFamily.HELVETICA, 12, Font.NORMAL);
        Paragraph paragraph = new Paragraph(text, bold);
        paragraph.setAlignment(alignment);
        return paragraph;
    }

    static Paragraph getParagraphBold(String text, int alignment) {
        Font bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
        Paragraph paragraph = new Paragraph(text);
        paragraph.setAlignment(alignment);
        return paragraph;
    }

    static void addOrderToDocument(Document document) throws DocumentException, IOException {

//        java.awt.Image qrCode = createQRCode();
//        com.itextpdf.text.Image qrCodeImage = com.itextpdf.text.Image.getInstance(qrCode, null);
//        qrCodeImage.scaleToFit(10, 10);
//
//        PdfPTable table = new PdfPTable(1);
//        table.setWidthPercentage(100);
//        PdfPCell cell;
//        table.getDefaultCell().setBorder(0);
//
//        cell = getCellBoldTitle("Viva kizomba congress Amsterdam '20 Goes to Brussels".toUpperCase(), Element.ALIGN_LEFT);
//
//        table.addCell(cell);
//        document.add(table);
//
//        PdfPTable qrCodAndDatetable = new PdfPTable(5);
//        qrCodAndDatetable.setSpacingBefore(50);
//        qrCodAndDatetable.setWidthPercentage(100);
//        qrCodAndDatetable.getDefaultCell().setBorder(Rectangle.TOP | Rectangle.BOTTOM);
//
//        qrCodAndDatetable.addCell(qrCodeImage);
//        qrCodAndDatetable.addCell(getCellWithPadding("Friday\n23 oct 2020".toUpperCase(), Element.ALIGN_CENTER));
//        qrCodAndDatetable.addCell(getCellWithPadding("Monday\n26 oct 2020".toUpperCase(), Element.ALIGN_CENTER));
//        qrCodAndDatetable.addCell(getCellWithPadding("Paris\nSaint-Denis, Stage de France".toUpperCase(), Element.ALIGN_LEFT));
//        qrCodAndDatetable.addCell(getCellWithPadding("", Element.ALIGN_LEFT));
//
//        document.add(qrCodAndDatetable);
//
//
//        PdfPTable lastTable = new PdfPTable(new float[]{4.6f, 0.6f, 4.7f});
//        lastTable.setSpacingBefore(20);
//        lastTable.setWidthPercentage(100);
//        lastTable.getDefaultCell().setBorder(0);
//
//        PdfPTable clientInfoTable = new PdfPTable(1);
//        clientInfoTable.getDefaultCell().setBorder(0);
//
//        clientInfoTable.addCell(getCellBoldClientName("Benjamin kafirongo", Element.ALIGN_LEFT));
//        clientInfoTable.addCell(getCell("Ticket type", Element.ALIGN_LEFT));
//        clientInfoTable.addCell(getCell("✍????NAME CHANGE / CHANGEMENT DE NOM", Element.ALIGN_LEFT));
//        clientInfoTable.addCell(getCell("ENGLISH\n" +
//            "ATTENTION, READ CAREFULLY THE CONDITIONS OF SALE OF OUR TICKETS.\n" +
//            "OUR UNIQUE PASS GIVES ACCESS TO : SOCIAL ROOM + THE 3 THEMED PARTIES: FRIDAY, SATURDAYDon't twice!\n" +
//            " print the same ticket\n" +
//            " N°\n" +
//            " 000046\n" +
//            "&\n", Element.ALIGN_LEFT));
//
//        PdfPTable conditionsTable = new PdfPTable(1);
//        conditionsTable.getDefaultCell().setBorder(0);
//
//        conditionsTable.addCell(getCell("Benjamin", Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, Rectangle.BOX));
//        cell = new PdfPCell();
//        cell.setBorder(Rectangle.NO_BORDER);
//        cell.setFixedHeight(10);
//        conditionsTable.addCell(cell);
//        cell = new PdfPCell();
//        cell.setBorder(Rectangle.NO_BORDER);
//        cell.setFixedHeight(100);
//        cell.setBackgroundColor(new GrayColor(150));
//        conditionsTable.addCell(cell);
//        conditionsTable.addCell(getCellBoldTitle("", Element.ALIGN_LEFT));
//
//
//        lastTable.addCell(clientInfoTable);
//        lastTable.addCell(getCell(" ", Element.ALIGN_CENTER));
//        lastTable.addCell(conditionsTable);
//        document.add(lastTable);

    }


    public static PdfPCell getCell(String text, int alignment) {
        PdfPCell cell = new PdfPCell(new Phrase(text, FontFactory.getFont(FontFactory.HELVETICA, 9, Font.NORMAL, BaseColor.BLACK)));
        cell.setPadding(0);
        cell.setHorizontalAlignment(alignment);
        cell.setPaddingRight(0);
        cell.setPaddingTop(1);
        cell.setPaddingBottom(1);
        cell.setBorder(PdfPCell.NO_BORDER);
        return cell;
    }

    public static PdfPCell getCellWithPadding(String text, int alignment) {
        PdfPCell cell = new PdfPCell(new Phrase(text, FontFactory.getFont(FontFactory.HELVETICA, 15, Font.NORMAL, BaseColor.BLACK)));
        cell.setPadding(0);
        cell.setHorizontalAlignment(alignment);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setPaddingRight(0);
//        cell.setPaddingTop(50);
//        cell.setPaddingBottom(1);
        cell.setBorder(PdfPCell.TOP | PdfPCell.BOTTOM);
        return cell;
    }

    public static PdfPCell getCellBoldTitle(String text, int alignment) {
        PdfPCell cell = new PdfPCell(new Phrase(text, FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLD, BaseColor.BLACK)));
        cell.setPadding(0);
        cell.setHorizontalAlignment(alignment);
        cell.setPaddingRight(0);
        cell.setPaddingTop(1);
        cell.setPaddingBottom(1);
        cell.setBorder(Rectangle.NO_BORDER);
        return cell;
    }

    public static PdfPCell getCellBoldClientName(String text, int alignment) {
        PdfPCell cell = new PdfPCell(new Phrase(text, FontFactory.getFont(FontFactory.TIMES_BOLD, 15, Font.BOLD, BaseColor.BLACK)));
        cell.setPadding(0);
        cell.setHorizontalAlignment(alignment);
        cell.setPaddingRight(0);
        cell.setPaddingTop(1);
        cell.setPaddingBottom(1);
        cell.setBorder(Rectangle.NO_BORDER);
        return cell;
    }


    public static PdfPCell getCell(String text, int horizontalAlignment, int verticaltalAlignment, int border) {
        PdfPCell cell = new PdfPCell(new Phrase(text, FontFactory.getFont(FontFactory.HELVETICA, 12, Font.NORMAL, BaseColor.BLACK)));
        cell.setPadding(0);
        cell.setHorizontalAlignment(horizontalAlignment);
        cell.setVerticalAlignment(verticaltalAlignment);
        cell.setPaddingRight(0);
        cell.setPaddingTop(10);
        cell.setPaddingBottom(10);
        cell.setBorder(border);
        return cell;
    }

    public static PdfPCell getCellBottomLine(String text, int alignment) {
        PdfPCell cell = new PdfPCell(new Phrase(text, FontFactory.getFont(FontFactory.HELVETICA, 9, Font.BOLD, BaseColor.BLACK)));
        cell.setPadding(0);
        cell.setHorizontalAlignment(alignment);
        cell.setPaddingTop(1);
        cell.setPaddingBottom(1);
        cell.setBorder(Rectangle.BOTTOM);
        return cell;
    }

    public static PdfPCell getCellTopLine(String text, int alignment) {
        PdfPCell cell = new PdfPCell(new Phrase(text, FontFactory.getFont(FontFactory.HELVETICA, 9, Font.BOLD, BaseColor.BLACK)));
        cell.setPadding(0);
        cell.setHorizontalAlignment(alignment);
        cell.setPaddingTop(1);
        cell.setPaddingBottom(1);
        cell.setBorder(Rectangle.TOP);
        return cell;
    }

    public static java.awt.Image createQRCode() {
        JSONObject parameters = new JSONObject();

        parameters.put("buyer", "43567865");

        System.out.println("parameters = " + parameters);
        Map<EncodeHintType, Object> qrParam = new HashMap<EncodeHintType, Object>();
        qrParam.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);
        qrParam.put(EncodeHintType.CHARACTER_SET, "UTF-8");
        BarcodeQRCode qrcode = new BarcodeQRCode(parameters.toString(), 100, 100, qrParam);
        return qrcode.createAwtImage(Color.BLACK, Color.WHITE);
    }
}


class DottedCell implements PdfPCellEvent {
    private int border = 0;

    public DottedCell(int border) {
        this.border = border;
    }


    public void cellLayout(PdfPCell cell, Rectangle position,
                           PdfContentByte[] canvases) {
        PdfContentByte canvas = canvases[PdfPTable.LINECANVAS];
        canvas.saveState();
        canvas.setLineDash(0, 4, 2);
        if ((border & PdfPCell.TOP) == PdfPCell.TOP) {
            canvas.moveTo(position.getRight(), position.getTop());
            canvas.lineTo(position.getLeft(), position.getTop());
        }
        if ((border & PdfPCell.BOTTOM) == PdfPCell.BOTTOM) {
            canvas.moveTo(position.getRight(), position.getBottom());
            canvas.lineTo(position.getLeft(), position.getBottom());
        }
        if ((border & PdfPCell.RIGHT) == PdfPCell.RIGHT) {
            canvas.moveTo(position.getRight(), position.getTop());
            canvas.lineTo(position.getRight(), position.getBottom());
        }
        if ((border & PdfPCell.LEFT) == PdfPCell.LEFT) {
            canvas.moveTo(position.getLeft(), position.getTop());
            canvas.lineTo(position.getLeft(), position.getBottom());
        }
        canvas.stroke();
        canvas.restoreState();
    }

}

class PDFEventListener extends PdfPageEventHelper {
    @Override
    public void onEndPage(PdfWriter writer, Document document) {
        PdfContentByte canvas = writer.getDirectContentUnder();
        Phrase watermark = new Phrase("TikTik", new Font(Font.FontFamily.TIMES_ROMAN, 50, Font.NORMAL, BaseColor.LIGHT_GRAY));
        ColumnText.showTextAligned(canvas, Element.ALIGN_CENTER, watermark, 200, 50, 45);
    }
}
