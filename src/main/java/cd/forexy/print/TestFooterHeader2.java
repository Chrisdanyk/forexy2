package cd.forexy.print;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;

import java.io.File;
import java.io.FileOutputStream;

public class TestFooterHeader2 extends PdfPageEventHelper {

    public static void main(String[] args) {
        try {

            String path = "/home/joe/Desktop/tetdocu.pdf";
            File file = new File(path);

            // create document
            Document document = new Document(PageSize.A4, 25, 25, 25, 176);

            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(file));

            // add header and footer
            TestFooterHeader2 eventPdf = new TestFooterHeader2();
            writer.setPageEvent(eventPdf);
            document.open();

            document.add(new Paragraph("benjamin".toUpperCase()));


            document.close();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private final Font footerFont = FontFactory.getFont(FontFactory.HELVETICA, 9, Font.BOLD, BaseColor.BLACK);
    private PdfTemplate t;
    private Image total;

    public void onOpenDocument(PdfWriter writer, Document document) {
        t = writer.getDirectContent().createTemplate(30, 16);
        try {
            total = Image.getInstance(t);
            total.setRole(PdfName.ARTIFACT);
        } catch (DocumentException de) {
            throw new ExceptionConverter(de);
        }
    }

    @Override
    public void onEndPage(PdfWriter writer, Document document) {

//        PdfContentByte canvas = writer.getDirectContent();
//        canvas.rectangle(25, 210, 530, 780);
//        canvas.setColorStroke(BaseColor.LIGHT_GRAY);
//        canvas.stroke();
//
//        PdfContentByte cb = writer.getDirectContent();
//        footerFont.setSize(15);
////        ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, new Phrase("____________________________________________________________",footerFont), ((document.left() + document.right())/2)+1f , document.bottom()+20, 0);
//        ColumnText.showTextAligned(cb, Element.ALIGN_CENTER, new Phrase(String.format("Page %d", writer.getPageNumber()),footerFont), (document.left() + document.right())/2 , document.bottom(), 0);

        addHeader(writer);
        addFooter(writer);
    }

    private void addHeader(PdfWriter writer) {
        PdfPTable header = new PdfPTable(2);
        try {
            // set defaults
            header.setSpacingBefore(2f);
            header.setWidths(new int[]{2, 24});
            header.setTotalWidth(527);
            header.setLockedWidth(true);
            header.getDefaultCell().setFixedHeight(40);
            header.getDefaultCell().setBorder(Rectangle.BOTTOM);
            header.getDefaultCell().setBorderColor(BaseColor.LIGHT_GRAY);

            // add image
//            Image logo = Image.getInstance(HeaderFooterPageEvent.class.getResource("/memorynotfound-logo.jpg"));
//            header.addCell(logo);

            // add text
            PdfPCell text = new PdfPCell();
            text.setPaddingBottom(15);
            text.setPaddingLeft(10);
            text.setBorder(Rectangle.BOTTOM);
            text.setBorderColor(BaseColor.LIGHT_GRAY);
            text.addElement(new Phrase("iText PDF Header Footer Example", new Font(Font.FontFamily.HELVETICA, 12)));
            text.addElement(new Phrase("https://memorynotfound.com", new Font(Font.FontFamily.HELVETICA, 8)));
            header.addCell(text);

            // write content
            header.writeSelectedRows(0, -1, 34, 803, writer.getDirectContent());
        } catch (DocumentException de) {
            throw new ExceptionConverter(de);
        } catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }

    private void addFooter(PdfWriter writer) {
        try {
            File logo = new File(TestMain.class.getResource("/images/IMG_2570.JPG").getFile());
            File logoShcnekina = new File(TestMain.class.getResource("/images/IMG_2570.JPG").getFile());

            Image imgLogo = Image.getInstance(logo.getPath());
            Image imgLogoSchnekina = Image.getInstance(logoShcnekina.getPath());
            imgLogoSchnekina.scalePercent(19f, 7f);
            PdfPTable footer = new PdfPTable(1);

            footer.setWidthPercentage(100);
            footer.getDefaultCell().setBorder(0);
            footer.setTotalWidth(525);
            footer.setHorizontalAlignment(Element.ALIGN_CENTER);

            float[] columnWidths = {3.5f, 3f, 3.5f};
            PdfPTable t = new PdfPTable(3);
            t.setWidths(columnWidths);
            t.setHorizontalAlignment(Element.ALIGN_CENTER);
            t.getDefaultCell().setBorder(0);

            t.addCell(new Paragraph(""));
            t.addCell(new Paragraph("\n"));
            t.addCell(new Paragraph(""));

            t.addCell(new Paragraph(""));
            t.addCell(imgLogoSchnekina);
            t.addCell(new Paragraph(""));
            footer.addCell(t);
            footer.addCell(getCell("Dr. Regina Sivieri – Anna-von-Schaden-Str. 1 – 93055 Regensburg – USt.-ID DE332511732", Element.ALIGN_CENTER));
            footer.addCell(getCell("Deutsche Kreditbank Berlin – IBAN: DE13120300001008003723 – BIC: BYLADEM1001", Element.ALIGN_CENTER));
            footer.addCell(getCell("schneckina@gmail.com – schneckina.etsy.com – www.schneckina.com", Element.ALIGN_CENTER));
            footer.addCell(imgLogo);

            // write page
            PdfContentByte canvas = writer.getDirectContent();
            canvas.beginMarkedContentSequence(PdfName.ARTIFACT);
            footer.writeSelectedRows(0, -1, 36, 195, canvas);
            canvas.endMarkedContentSequence();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void onCloseDocument(PdfWriter writer, Document document) {
        int totalLength = String.valueOf(writer.getPageNumber()).length();
        int totalWidth = totalLength * 5;
        ColumnText.showTextAligned(t, Element.ALIGN_RIGHT,
            new Phrase(String.valueOf(writer.getPageNumber()), new Font(Font.FontFamily.HELVETICA, 8)),
            totalWidth, 6, 0);
    }

    public static PdfPCell getCell(String text, int alignment) {
        PdfPCell cell = new PdfPCell(new Phrase(text, FontFactory.getFont(FontFactory.HELVETICA, 9, Font.NORMAL, BaseColor.BLACK)));
        cell.setPadding(0);
        cell.setHorizontalAlignment(alignment);
        cell.setPaddingRight(0);
        cell.setPaddingTop(1);
        cell.setPaddingBottom(1);
        cell.setBorder(PdfPCell.NO_BORDER);
        return cell;
    }

    public static PdfPCell getCell2(String text, int alignment) {
        PdfPCell cell = new PdfPCell(new Phrase(text, FontFactory.getFont(FontFactory.TIMES, 8, Font.NORMAL, BaseColor.BLACK)));
        cell.setPadding(0);
        cell.setHorizontalAlignment(alignment);
        cell.setPaddingRight(0);
        cell.setBorder(Rectangle.NO_BORDER);
        return cell;
    }


}
