package cd.forexy.domain;

public enum ContratStatut {
    
    ACTIF, INACTIF;
}
