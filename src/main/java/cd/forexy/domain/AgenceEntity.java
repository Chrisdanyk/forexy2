package cd.forexy.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

@Entity(name="Agence")
@Table(name="\"AGENCE\"")
@XmlRootElement
public class AgenceEntity extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Size(max = 50)
    @Column(length = 50, name="\"nom\"")
    private String nom;

    @Size(max = 50)
    @Column(length = 50, name="\"idNat\"")
    private String idNat;

    @Size(max = 50)
    @Column(length = 50, name="\"rcCM\"")
    private String rcCM;

    @Size(max = 50)
    @Column(length = 50, name="\"telephone\"")
    private String telephone;

    @Size(max = 50)
    @Column(length = 50, name="\"email\"")
    private String email;

    @Size(max = 255)
    @Column(length = 255, name="\"adresse\"")
    private String adresse;

    @Size(max = 50)
    @Column(length = 50, name="\"agenceID\"")
    @NotNull
    private String agenceID;

    @Column(name="\"STATUT\"")
    @Enumerated(EnumType.STRING)
    private AgenceStatut statut;

    @Size(max = 255)
    @Column(length = 255, name="\"province\"")
    private String province;

    @Size(max = 255)
    @Column(length = 255, name="\"ville\"")
    private String ville;

    @Size(max = 255)
    @Column(length = 255, name="\"avenue\"")
    private String avenue;

    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getIdNat() {
        return this.idNat;
    }

    public void setIdNat(String idNat) {
        this.idNat = idNat;
    }

    public String getRcCM() {
        return this.rcCM;
    }

    public void setRcCM(String rcCM) {
        this.rcCM = rcCM;
    }

    public String getTelephone() {
        return this.telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAdresse() {
        return this.adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getAgenceID() {
        return this.agenceID;
    }

    public void setAgenceID(String agenceID) {
        this.agenceID = agenceID;
    }

    public AgenceStatut getStatut() {
        return statut;
    }

    public void setStatut(AgenceStatut statut) {
        this.statut = statut;
    }

    public String getProvince() {
        return this.province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getVille() {
        return this.ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getAvenue() {
        return this.avenue;
    }

    public void setAvenue(String avenue) {
        this.avenue = avenue;
    }

}
