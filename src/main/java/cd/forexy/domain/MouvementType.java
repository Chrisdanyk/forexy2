package cd.forexy.domain;

public enum MouvementType {
    
    ENTREE, SORTIE;
}
