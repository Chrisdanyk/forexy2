package cd.forexy.domain;

import cd.forexy.domain.security.UserEntity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

@Entity(name="Mouvement")
@Table(name="\"MOUVEMENT\"")
@XmlRootElement
public class MouvementEntity extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name="\"date\"")
    @Temporal(TemporalType.DATE)
    @NotNull
    private Date date;

    @Size(max = 255)
    @Column(length = 255, name="\"motif\"")
    private String motif;

    @Column(name="\"TYPE\"")
    @Enumerated(EnumType.STRING)
    private MouvementType type;

    @ManyToOne(optional=true)
    @JoinColumn(name = "USER_ID", referencedColumnName = "ID")
    private UserEntity user;

    @ManyToOne(optional=true)
    @JoinColumn(name = "AGENCE_ID", referencedColumnName = "ID")
    private AgenceEntity agence;

    public Date getDate() {
        return this.date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getMotif() {
        return this.motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    public MouvementType getType() {
        return type;
    }

    public void setType(MouvementType type) {
        this.type = type;
    }

    public UserEntity getUser() {
        return this.user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public AgenceEntity getAgence() {
        return this.agence;
    }

    public void setAgence(AgenceEntity agence) {
        this.agence = agence;
    }

}
