package cd.forexy.domain;

public enum AgenceStatut {
    
    ACTIF, INACTIF;
}
