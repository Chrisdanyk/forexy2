package cd.forexy.domain;

public enum ContratPiece {
    
    PASSEPORT, PERMISDECONDUIRE, CARTEELECTEUR;
}
