package cd.forexy.domain;

public enum ContratCivilite {
    
    MONSIEUR, MADAME;
}
