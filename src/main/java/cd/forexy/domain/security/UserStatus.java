package cd.forexy.domain.security;

/**
 * User account status
 * */
public enum UserStatus {

    Active, Disabled, NotConfirmed, RegistrationError
}
