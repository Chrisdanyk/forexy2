package cd.forexy.domain.security;

/**
 * User role
 * */
public enum UserRole {

    Administrator, Daf, Dg, Agent, AgentReception, Registered;
}
