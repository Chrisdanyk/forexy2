package cd.forexy.domain.security;

public enum UserStatutMarital {

    CELIBATAIRE, MARIEE, VEUFVE, DIVORCEE;
}
