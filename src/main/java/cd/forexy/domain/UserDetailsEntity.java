package cd.forexy.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity(name="UserDetails")
@Table(name="\"USERDETAILS\"")
@XmlRootElement
public class UserDetailsEntity extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @OneToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    private UserDetailsImage image;
    
    @Size(max = 50)
    @Column(length = 50, name="\"telephone\"")
    private String telephone;

    @XmlTransient
    public UserDetailsImage getImage() {
        return image;
    }

    public void setImage(UserDetailsImage image) {
        this.image = image;
    }
    
    public String getTelephone() {
        return this.telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

}
