package cd.forexy.domain;
import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name="ContratAttachment")
public class ContratAttachment extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    public ContratAttachment() {
        super();
    }
    
    public ContratAttachment(Long id, String fileName) {
        this.setId(id);
        this.fileName = fileName;
    }

    @Size(max = 200)
    private String fileName;
    
    @ManyToOne
    @JoinColumn(name = "CONTRAT_ID", referencedColumnName = "ID")
    private ContratEntity contrat;

    @Lob
    private byte[] content;

    public byte[] getContent() {
        return this.content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }
    
    public String getFileName() {
        return this.fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public ContratEntity getContrat() {
        return this.contrat;
    }

    public void setContrat(ContratEntity contrat) {
        this.contrat = contrat;
    }
}
