package cd.forexy.domain;

import cd.forexy.domain.security.UserEntity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity(name="Contrat")
@Table(name="\"CONTRAT\"")
@XmlRootElement
public class ContratEntity extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @OneToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    private ContratImage image;
    
    @Column(name="\"CIVILITE\"")
    @Enumerated(EnumType.STRING)
    private ContratCivilite civilite;

    @Column(name="\"PIECE\"")
    @Enumerated(EnumType.STRING)
    private ContratPiece piece;

    @Digits(integer = 8,  fraction = 2)
    @Column(precision = 10, scale = 2, name="\"montant\"")
    @NotNull
    private BigDecimal montant;

    @Size(max = 50)
    @Column(length = 50, name="\"bordereauID\"")
    @NotNull
    private String bordereauID;

    @Size(max = 50)
    @Column(length = 50, name="\"recuID\"")
    @NotNull
    private String recuID;

    @ManyToOne(optional=true)
    @JoinColumn(name = "AGENCE_ID", referencedColumnName = "ID")
    private AgenceEntity agence;

    @Column(name="\"date\"")
    @Temporal(TemporalType.DATE)
    @NotNull
    private Date date;

    @Size(max = 50)
    @Column(length = 50, name="\"nom\"")
    private String nom;

    @Size(max = 50)
    @Column(length = 50, name="\"postNom\"")
    private String postNom;

    @Size(max = 50)
    @Column(length = 50, name="\"prenom\"")
    private String prenom;

    @Size(max = 50)
    @Column(length = 50, name="\"adresse\"")
    private String adresse;

    @Column(name="\"dateNaissance\"")
    @Temporal(TemporalType.DATE)
    private Date dateNaissance;

    @Size(max = 50)
    @Column(length = 50, name="\"lieuNaissance\"")
    private String lieuNaissance;

    @Size(max = 50)
    @Column(length = 50, name="\"telephone\"")
    @NotNull
    private String telephone;

    @Size(max = 50)
    @Column(length = 50, name="\"email\"")
    private String email;

    @Size(max = 50)
    @Column(length = 50, name="\"contratID\"")
    private String contratID;

    @ManyToOne(optional=true)
    @JoinColumn(name = "USER_ID", referencedColumnName = "ID")
    private UserEntity user;

    @Column(name="\"STATUT\"")
    @Enumerated(EnumType.STRING)
    private ContratStatut statut;

    @ManyToOne(optional=true)
    @JoinColumn(name = "AGENCE2_ID", referencedColumnName = "ID")
    private AgenceEntity agence2;

    @Column(name="\"contratNumber\"")
    @Digits(integer = 4, fraction = 0)
    private Integer contratNumber;

    @Column(name="\"dateEcheance1\"")
    @Temporal(TemporalType.DATE)
    @NotNull
    private Date dateEcheance1;

    @Column(name="\"dateEcheance2\"")
    @Temporal(TemporalType.DATE)
    @NotNull
    private Date dateEcheance2;

    @Column(name="\"dateEcheance3\"")
    @Temporal(TemporalType.DATE)
    @NotNull
    private Date dateEcheance3;

    @Column(name="\"echeance1Payed\"")
    private Boolean echeance1Payed;

    @Column(name="\"echeance2Payed\"")
    private Boolean echeance2Payed;

    @Column(name="\"echeance3Payed\"")
    private Boolean echeance3Payed;

    @Column(name = "ENTRY_CREATED_BY")
    private String createdBy;

    @Column(name = "ENTRY_CREATED_AT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @Column(name = "ENTRY_MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "ENTRY_MODIFIED_AT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedAt;
    
    @XmlTransient
    public ContratImage getImage() {
        return image;
    }

    public void setImage(ContratImage image) {
        this.image = image;
    }
    
    public ContratCivilite getCivilite() {
        return civilite;
    }

    public void setCivilite(ContratCivilite civilite) {
        this.civilite = civilite;
    }

    public ContratPiece getPiece() {
        return piece;
    }

    public void setPiece(ContratPiece piece) {
        this.piece = piece;
    }

    public BigDecimal getMontant() {
        return this.montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public String getBordereauID() {
        return this.bordereauID;
    }

    public void setBordereauID(String bordereauID) {
        this.bordereauID = bordereauID;
    }

    public String getRecuID() {
        return this.recuID;
    }

    public void setRecuID(String recuID) {
        this.recuID = recuID;
    }

    public AgenceEntity getAgence() {
        return this.agence;
    }

    public void setAgence(AgenceEntity agence) {
        this.agence = agence;
    }

    public Date getDate() {
        return this.date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPostNom() {
        return this.postNom;
    }

    public void setPostNom(String postNom) {
        this.postNom = postNom;
    }

    public String getPrenom() {
        return this.prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getAdresse() {
        return this.adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public Date getDateNaissance() {
        return this.dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getLieuNaissance() {
        return this.lieuNaissance;
    }

    public void setLieuNaissance(String lieuNaissance) {
        this.lieuNaissance = lieuNaissance;
    }

    public String getTelephone() {
        return this.telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContratID() {
        return this.contratID;
    }

    public void setContratID(String contratID) {
        this.contratID = contratID;
    }

    public UserEntity getUser() {
        return this.user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public ContratStatut getStatut() {
        return statut;
    }

    public void setStatut(ContratStatut statut) {
        this.statut = statut;
    }

    public AgenceEntity getAgence2() {
        return this.agence2;
    }

    public void setAgence2(AgenceEntity agence) {
        this.agence2 = agence;
    }

    public Integer getContratNumber() {
        return this.contratNumber;
    }

    public void setContratNumber(Integer contratNumber) {
        this.contratNumber = contratNumber;
    }

    public Date getDateEcheance1() {
        return this.dateEcheance1;
    }

    public void setDateEcheance1(Date dateEcheance1) {
        this.dateEcheance1 = dateEcheance1;
    }

    public Date getDateEcheance2() {
        return this.dateEcheance2;
    }

    public void setDateEcheance2(Date dateEcheance2) {
        this.dateEcheance2 = dateEcheance2;
    }

    public Date getDateEcheance3() {
        return this.dateEcheance3;
    }

    public void setDateEcheance3(Date dateEcheance3) {
        this.dateEcheance3 = dateEcheance3;
    }

    public Boolean getEcheance1Payed() {
        return this.echeance1Payed;
    }

    public void setEcheance1Payed(Boolean echeance1Payed) {
        this.echeance1Payed = echeance1Payed;
    }

    public Boolean getEcheance2Payed() {
        return this.echeance2Payed;
    }

    public void setEcheance2Payed(Boolean echeance2Payed) {
        this.echeance2Payed = echeance2Payed;
    }

    public Boolean getEcheance3Payed() {
        return this.echeance3Payed;
    }

    public void setEcheance3Payed(Boolean echeance3Payed) {
        this.echeance3Payed = echeance3Payed;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public Date getModifiedAt() {
        return modifiedAt;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }
    
    public void updateAuditInformation(String username) {
        if (this.getId() != null) {
            modifiedAt = new Date();
            modifiedBy = username;
        } else {
            createdAt = new Date();
            modifiedAt = createdAt;
            createdBy = username;
            modifiedBy = createdBy;
        }
    }
    
}
