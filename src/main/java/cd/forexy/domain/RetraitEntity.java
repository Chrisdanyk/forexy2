package cd.forexy.domain;

import cd.forexy.domain.security.UserEntity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

@Entity(name="Retrait")
@Table(name="\"RETRAIT\"")
@XmlRootElement
public class RetraitEntity extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Digits(integer = 8,  fraction = 2)
    @Column(precision = 10, scale = 2, name="\"montant\"")
    @NotNull
    private BigDecimal montant;

    @ManyToOne(optional=true)
    @JoinColumn(name = "CONTRAT_ID", referencedColumnName = "ID")
    private ContratEntity contrat;

    @Column(name="\"date\"")
    @Temporal(TemporalType.DATE)
    @NotNull
    private Date date;

    @ManyToOne(optional=true)
    @JoinColumn(name = "USER_ID", referencedColumnName = "ID")
    private UserEntity user;

    @ManyToOne(optional=true)
    @JoinColumn(name = "AGENCE_ID", referencedColumnName = "ID")
    private AgenceEntity agence;

    @Column(name="\"STATUS\"")
    @Enumerated(EnumType.STRING)
    private RetraitStatus status;

    @Column(name="\"ECHEANCE\"")
    @Enumerated(EnumType.STRING)
    private RetraitEcheance echeance;

    @Column(name = "ENTRY_CREATED_BY")
    private String createdBy;

    @Column(name = "ENTRY_CREATED_AT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @Column(name = "ENTRY_MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "ENTRY_MODIFIED_AT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedAt;
    
    public BigDecimal getMontant() {
        return this.montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public ContratEntity getContrat() {
        return this.contrat;
    }

    public void setContrat(ContratEntity contrat) {
        this.contrat = contrat;
    }

    public Date getDate() {
        return this.date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public UserEntity getUser() {
        return this.user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public AgenceEntity getAgence() {
        return this.agence;
    }

    public void setAgence(AgenceEntity agence) {
        this.agence = agence;
    }

    public RetraitStatus getStatus() {
        return status;
    }

    public void setStatus(RetraitStatus status) {
        this.status = status;
    }

    public RetraitEcheance getEcheance() {
        return echeance;
    }

    public void setEcheance(RetraitEcheance echeance) {
        this.echeance = echeance;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public Date getModifiedAt() {
        return modifiedAt;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }
    
    public void updateAuditInformation(String username) {
        if (this.getId() != null) {
            modifiedAt = new Date();
            modifiedBy = username;
        } else {
            createdAt = new Date();
            modifiedAt = createdAt;
            createdBy = username;
            modifiedBy = createdBy;
        }
    }
    
}
