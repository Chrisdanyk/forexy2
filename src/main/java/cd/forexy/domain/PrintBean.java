package cd.forexy.domain;

import cd.forexy.domain.ContratEntity;
import cd.forexy.domain.ContratPiece;
import cd.forexy.test.NumberToWordConverter;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;

import javax.faces.view.ViewScoped;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;

public class PrintBean implements Serializable {

    public static final String PIECE_SELECTOR = "[x]";
    private static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    private static final Font regular = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);
    private static final Font bold = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);

    public static void addContractToDocument(ContratEntity contratEntity, Document document) throws DocumentException, IOException {

        Phrase p;
        p = new Phrase("\nContrat d’investissement\n", bold);
        p.add(new Chunk(contratEntity.getContratID() + "\n\n", bold));
        Paragraph paragraph = new Paragraph();
        paragraph.setAlignment(Element.ALIGN_CENTER);
        paragraph.add(p);
        document.add(paragraph);

        paragraph = new Paragraph();
        paragraph.setAlignment(Element.ALIGN_JUSTIFIED);
        p = new Phrase("", regular);
        p.add(new Chunk("Conclu entre\n\n", bold));
        p.add(new Chunk("FOREXY GROUP Sarl, ", bold));
        p.add(new Chunk("immatriculé au registre de commerce et du crédit mobilier de " +
            "Lubumbashi sous le numéro ", regular));
        p.add(new Chunk("CD/LSH/RCCM/21-B-00017, idnat 05-H5300-N71610W", bold));
        p.add(new Chunk(" et ayant son siège au 20 av Champ du potier, C/ Lubumbashi, V/Lubumbashi, P/Haut-Katanga, " +
            "Représenté aux fins des présentes par ", regular));
        p.add(new Chunk("Monsieur ", bold));
        p.add(new Chunk("LUKEKA ", regular));
        p.add(new Chunk("MWANGA Hugues, ", bold));
        p.add(new Chunk("Directeur Général, et WAMU  ", regular));
        p.add(new Chunk("TYANZA Phibert Furet, ", bold));
        p.add(new Chunk("Directeur Administratif et Financier, ci-après dénommés les mandataires.\n\n", regular));

        p.add(new Chunk("Et\n", regular));
        p.add(new Chunk("Monsieur / Madame : ", regular));
        p.add(new Chunk(contratEntity.getNom() + " " + contratEntity.getPrenom() + "" + "\n\n", bold));
        p.add(new Chunk("Résident au : ", regular));
        p.add(new Chunk(contratEntity.getAdresse() + "" + " \n\n", bold));
        p.add(new Chunk("République Démocratique du Congo, ci-aprés dénommé le mandant.\n\n", regular));
        p.add(new Chunk("Piéce d'identité présentée :\n", regular));
        p.add(new Chunk("Passeport " + (contratEntity.getPiece() == ContratPiece.PASSEPORT ? PIECE_SELECTOR : "") + "\n", regular));
        p.add(new Chunk("Permis de conduire " + (contratEntity.getPiece() == ContratPiece.PERMISDECONDUIRE ? PIECE_SELECTOR : "") + "\n", regular));
        p.add(new Chunk("Carte nationale d’électeur " + (contratEntity.getPiece() == ContratPiece.CARTEELECTEUR ? PIECE_SELECTOR : "") + "\n", regular));
        p.add(new Chunk("Numéro de téléphone : " + contratEntity.getTelephone() + "\n\n", regular));
        p.add(new Chunk("Montant investi : ", regular));
        p.add(new Chunk(contratEntity.getMontant() + "$", bold));
        p.add(new Chunk(", en toutes lettres ", regular));
        p.add(new Chunk(NumberToWordConverter.numberToWordFR(contratEntity.getMontant().intValue()) + " dollars americains\n\n", bold));
//
        p.add(new Chunk("Préambule\n", bold));
        p.add(new Chunk("Les présentes clauses contractuelles servent de base a l'accord conclu entre le Mandant et le Mandataire," +
            " pour créer une collaboration confiante et constructive de part et d’autre.\n\n", regular));

        p.add(new Chunk("I. Droits et obligations découlant du Contrat\n", bold));
        p.add(new Chunk("Les conventions définies par écrit sont seules applicables au contrat conclu entre Le Mandant et le Mandataire.\n", regular));
        p.add(new Chunk("Les modifications, accords ou promesses doivent etre confirmés par écrit. \n\n", regular));
//
        p.add(new Chunk("II. Mandat de gestion\n", bold));
        p.add(new Chunk("Le Mandant charge le Mandataire de la gestion de son capital investi, déposé auprés " +
            "d'une Banque choisie par le Mandataire. II n'est pas nécessaire que le Mandant donne son accord spécial pour " +
            "les opérations commerciales.\n\n", regular));
//
        p.add(new Chunk("III. Entendue du mandat\n", bold));
        p.add(new Chunk("Le Mandataire est seul et exclusivement habilité, par le pouvoir conféré par le Mandat à acquérir des devises et autres instruments financiers,  à son gré," +
            "  à vendre tous les titres et devises dont la vente lui paraitra opportuns, et en réinvestir le produit  à son gré.\n" +
            "Le Mandataire n'est pas habilité à effectuer des retraits ou à disposer autrement du compte du Mandant pour son propre compte ou en faveur de tiers.\n\n", regular));

        p.add(new Chunk("IV. Dénonciation et durée du Contrat\n", bold));
        p.add(new Chunk("Le présent contrat entre en vigueur au moment du versement de la somme a investir sur le compte désigné par" +
            " le Mandataire ou au guichet de FOREXY GROUP Sarl contre un bordereau de versement indiquant le montant et portant le numéro du présent contrat.\n\n" +
            "Il est conclu pour une durée ", regular));
        p.add(new Chunk("d’une année ", bold));
        p.add(new Chunk("et doit étre renouvelé par la signature d'un nouveau contrat.\n\n", regular));
        p.add(new Chunk("Le Contrat peut étre dénoncé à tout moment à la fin de l'échéance d'", regular));
        p.add(new Chunk("une année (12 mois) ", bold));
        p.add(new Chunk("à la demande de l'une des parties. \n", regular));
        p.add(new Chunk("De ce fait une pénalité de ", regular));
        p.add(new Chunk(" 35% ", bold));
        p.add(new Chunk("sera débitée au capital initial (capital investi) pour ceux qui voudront récupérer leur argent avant l’échéance d’", regular));
        p.add(new Chunk("une année (12 mois).\n\n", bold));
        p.add(new Chunk("Le présent mandat est maintenu sous réserve de circonstances contraignantes telles que décés, " +
            "déclarations de disparition, ou incapacité ou faillite du Mandataire.\n\n", regular));

        p.add(new Chunk("En cas d'empéchement, le mandat peut déléguer une personne moyennant une procuration.\n\n", regular));
        p.add(new Chunk("V. Rémunération\n", bold));
        p.add(new Chunk("Le Mandataire garanti au Mandant une performance de ", regular));
        p.add(new Chunk("25% ", bold));
        p.add(new Chunk("de gain par mois, soit ", regular));
        p.add(new Chunk("100% ", bold));
        p.add(new Chunk("de bénéfice chaque ", regular));
        p.add(new Chunk("quatre mois ", bold));
        p.add(new Chunk("du capital total investi. Le capital investit est maintenu pour une durée d’", regular));
        p.add(new Chunk("une année (12 mois).\n\n", bold));
        p.add(new Chunk("Le bénéfice est débité directement chaque fin de l'échéance de quatre mois selon le mode" +
            " de payement choisi par le Mandant. Les éventuelles rétrocessions et autres rémunérations des tiers reviennent au Mandataire.\n", bold));
        p.add(new Chunk("Les bénéfices générés ne sont en aucune circonstance automatiquement réinvestis.\n\n", bold));

        p.add(new Chunk("VI. Divers\n", bold));
        p.add(new Chunk("Aucun accord oral n'est conclu en dehors du présent contrat. Tout complement ou modification du contrat," +
            " ou toute convention concernant son annulation exige pour etre valable la forme écrite.\n\n", bold));

        p.add(new Chunk("Si l'une quelconque des dispositions du présent contrat s'avére par la suite invalide" +
            " pour un motif Juridique, les autres dispositions n'en seront pas affectées.\n" +
            "\n" +
            "Au lieu et Place de la disposition invalide, sera considérée comme convenue " +
            "toute disposition valide se rapprochant le plus de lesprit et du but de la disposition invalide, décidée " +
            "de bonne foi, conformément aux bonnes pratiques commerciales et aux us et coutumes habituels dans les opérations de cette " +
            "nature.\n" +
            "La même disposition s'applique à une lacune éventuelle du contrat. Tous les droits découlant de ce contrat " +
            "expirent en cas de résiliation légale par l’une des parties.\n\n", regular));

        p.add(new Chunk("VII. Connaissance des risques\n", bold));
        p.add(new Chunk("Le mandant confirme par sa signature qu'il a dûment été informé complétement et " +
            "suffisamment des risques de pertes et des possibilités de gains liés au commerce des devises et autres" +
            " placements possibles prévus au paragraphe III, et du fait que le Mandataire assume toute " +
            "responsabilité à cet égard.\n\n\n\n", regular));
        p.add(new Chunk("VIII. Des différends\n", bold));
        p.add(new Chunk("Tout différend qui pourrait résulter de I'interprétation ou de l'exécution du présent" +
            " contrat est réglé à l'amiable, en cas de désaccord persistant le litige sera soumis au tribunal compétent selon" +
            " les lois en vigueur en République Démocratique du Congo.\n\n", regular));

        p.add(new Chunk("Pour le compte de FOREXY GROUP\n\n", regular));
        paragraph.add(p);
        document.add(paragraph);

        float[] columnWidths = {5f, 5f};
        PdfPTable bossNamesTable = new PdfPTable(2);

        bossNamesTable.setTotalWidth(527);
        bossNamesTable.setLockedWidth(true);
        bossNamesTable.setWidthPercentage(100);
        bossNamesTable.getDefaultCell().setBorder(Rectangle.NO_BORDER);

        bossNamesTable.addCell(getCellBold("LUKEKA MWANGA Hugues", Element.ALIGN_LEFT));
        bossNamesTable.addCell(getCellBold("WAMU IYANZA Phibert Furet", Element.ALIGN_RIGHT));

        bossNamesTable.addCell(getCellBold("Directeur Général", Element.ALIGN_LEFT));
        bossNamesTable.addCell(getCell("Directeur Administratif et Financier", Element.ALIGN_RIGHT));

        document.add(bossNamesTable);

        paragraph = new Paragraph();
        paragraph.setAlignment(Element.ALIGN_LEFT);
        p = new Phrase("\n\n\n\n\n\n\nSignature du Mandant\n\n\n\n\n\n\n", regular);
        paragraph.add(p);
        document.add(paragraph);

        paragraph = new Paragraph();
        paragraph.setAlignment(Element.ALIGN_RIGHT);
        p = new Phrase("Fait à " + contratEntity.getAgence().getVille() != null ? contratEntity.getAgence().getVille() : "Town", regular);
        p.add(new Chunk(", le " + sdf.format(contratEntity.getDate()), regular));

        paragraph.add(p);
        document.add(paragraph);

    }

    public static PdfPCell getCell(String text, int alignment) {
        PdfPCell cell = new PdfPCell(new Phrase(text, regular));
        cell.setPadding(0);
        cell.setHorizontalAlignment(alignment);
        cell.setPaddingRight(0);
        cell.setPaddingTop(1);
        cell.setPaddingBottom(1);
        cell.setBorder(PdfPCell.NO_BORDER);
        return cell;
    }

    public static PdfPCell getCellBold(String text, int alignment) {
        PdfPCell cell = new PdfPCell(new Phrase(text, bold));
        cell.setPadding(0);
        cell.setHorizontalAlignment(alignment);
        cell.setPaddingRight(0);
        cell.setPaddingTop(1);
        cell.setPaddingBottom(1);
        cell.setBorder(PdfPCell.NO_BORDER);
        return cell;
    }

}

class DottedCell implements PdfPCellEvent {
    private int border = 0;

    public DottedCell(int border) {
        this.border = border;
    }

    public void cellLayout(PdfPCell cell, Rectangle position,
                           PdfContentByte[] canvases) {
        PdfContentByte canvas = canvases[PdfPTable.LINECANVAS];
        canvas.saveState();
        canvas.setLineDash(0, 4, 2);
        if ((border & PdfPCell.TOP) == PdfPCell.TOP) {
            canvas.moveTo(position.getRight(), position.getTop());
            canvas.lineTo(position.getLeft(), position.getTop());
        }
        if ((border & PdfPCell.BOTTOM) == PdfPCell.BOTTOM) {
            canvas.moveTo(position.getRight(), position.getBottom());
            canvas.lineTo(position.getLeft(), position.getBottom());
        }
        if ((border & PdfPCell.RIGHT) == PdfPCell.RIGHT) {
            canvas.moveTo(position.getRight(), position.getTop());
            canvas.lineTo(position.getRight(), position.getBottom());
        }
        if ((border & PdfPCell.LEFT) == PdfPCell.LEFT) {
            canvas.moveTo(position.getLeft(), position.getTop());
            canvas.lineTo(position.getLeft(), position.getBottom());
        }
        canvas.stroke();
        canvas.restoreState();
    }

}

class PDFEventListener extends PdfPageEventHelper {
    @Override
    public void onEndPage(PdfWriter writer, Document document) {
        PdfContentByte canvas = writer.getDirectContentUnder();
        Phrase watermark = new Phrase("Forexy", new Font(Font.FontFamily.TIMES_ROMAN, 60, Font.NORMAL, BaseColor.LIGHT_GRAY));
        ColumnText.showTextAligned(canvas, Element.ALIGN_CENTER, watermark, 200, 50, 45);
    }
}
