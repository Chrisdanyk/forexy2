package cd.forexy.rest;

import cd.forexy.domain.ContratEntity;
import cd.forexy.service.ContratService;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/contrats")
@Named
public class ContratResource implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Inject
    private ContratService contratService;
    
    /**
     * Get the complete list of Contrat Entries <br/>
     * HTTP Method: GET <br/>
     * Example URL: /contrats
     * @return List of ContratEntity (JSON)
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<ContratEntity> getAllContrats() {
        return contratService.findAllContratEntities();
    }
    
    /**
     * Get the number of Contrat Entries <br/>
     * HTTP Method: GET <br/>
     * Example URL: /contrats/count
     * @return Number of ContratEntity
     */
    @GET
    @Path("count")
    @Produces(MediaType.APPLICATION_JSON)
    public long getCount() {
        return contratService.countAllEntries();
    }
    
    /**
     * Get a Contrat Entity <br/>
     * HTTP Method: GET <br/>
     * Example URL: /contrats/3
     * @param id
     * @return A Contrat Entity (JSON)
     */
    @Path("{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ContratEntity getContratById(@PathParam("id") Long id) {
        return contratService.find(id);
    }
    
    /**
     * Create a Contrat Entity <br/>
     * HTTP Method: POST <br/>
     * POST Request Body: New ContratEntity (JSON) <br/>
     * Example URL: /contrats
     * @param contrat
     * @return A ContratEntity (JSON)
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ContratEntity addContrat(ContratEntity contrat) {
        return contratService.save(contrat);
    }
    
    /**
     * Update an existing Contrat Entity <br/>
     * HTTP Method: PUT <br/>
     * PUT Request Body: Updated ContratEntity (JSON) <br/>
     * Example URL: /contrats
     * @param contrat
     * @return A ContratEntity (JSON)
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ContratEntity updateContrat(ContratEntity contrat) {
        return contratService.update(contrat);
    }
    
    /**
     * Delete an existing Contrat Entity <br/>
     * HTTP Method: DELETE <br/>
     * Example URL: /contrats/3
     * @param id
     */
    @Path("{id}")
    @DELETE
    public void deleteContrat(@PathParam("id") Long id) {
        ContratEntity contrat = contratService.find(id);
        contratService.delete(contrat);
    }
    
}
