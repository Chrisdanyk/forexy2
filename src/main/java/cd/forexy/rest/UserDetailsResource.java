package cd.forexy.rest;

import cd.forexy.domain.UserDetailsEntity;
import cd.forexy.service.UserDetailsService;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/userDetailss")
@Named
public class UserDetailsResource implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Inject
    private UserDetailsService userDetailsService;
    
    /**
     * Get the complete list of UserDetails Entries <br/>
     * HTTP Method: GET <br/>
     * Example URL: /userDetailss
     * @return List of UserDetailsEntity (JSON)
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UserDetailsEntity> getAllUserDetailss() {
        return userDetailsService.findAllUserDetailsEntities();
    }
    
    /**
     * Get the number of UserDetails Entries <br/>
     * HTTP Method: GET <br/>
     * Example URL: /userDetailss/count
     * @return Number of UserDetailsEntity
     */
    @GET
    @Path("count")
    @Produces(MediaType.APPLICATION_JSON)
    public long getCount() {
        return userDetailsService.countAllEntries();
    }
    
    /**
     * Get a UserDetails Entity <br/>
     * HTTP Method: GET <br/>
     * Example URL: /userDetailss/3
     * @param id
     * @return A UserDetails Entity (JSON)
     */
    @Path("{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public UserDetailsEntity getUserDetailsById(@PathParam("id") Long id) {
        return userDetailsService.find(id);
    }
    
    /**
     * Create a UserDetails Entity <br/>
     * HTTP Method: POST <br/>
     * POST Request Body: New UserDetailsEntity (JSON) <br/>
     * Example URL: /userDetailss
     * @param userDetails
     * @return A UserDetailsEntity (JSON)
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UserDetailsEntity addUserDetails(UserDetailsEntity userDetails) {
        return userDetailsService.save(userDetails);
    }
    
    /**
     * Update an existing UserDetails Entity <br/>
     * HTTP Method: PUT <br/>
     * PUT Request Body: Updated UserDetailsEntity (JSON) <br/>
     * Example URL: /userDetailss
     * @param userDetails
     * @return A UserDetailsEntity (JSON)
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UserDetailsEntity updateUserDetails(UserDetailsEntity userDetails) {
        return userDetailsService.update(userDetails);
    }
    
    /**
     * Delete an existing UserDetails Entity <br/>
     * HTTP Method: DELETE <br/>
     * Example URL: /userDetailss/3
     * @param id
     */
    @Path("{id}")
    @DELETE
    public void deleteUserDetails(@PathParam("id") Long id) {
        UserDetailsEntity userDetails = userDetailsService.find(id);
        userDetailsService.delete(userDetails);
    }
    
}
