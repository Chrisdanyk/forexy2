package cd.forexy.rest;

import cd.forexy.domain.ParametreEntity;
import cd.forexy.service.ParametreService;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/parametres")
@Named
public class ParametreResource implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Inject
    private ParametreService parametreService;
    
    /**
     * Get the complete list of Parametre Entries <br/>
     * HTTP Method: GET <br/>
     * Example URL: /parametres
     * @return List of ParametreEntity (JSON)
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<ParametreEntity> getAllParametres() {
        return parametreService.findAllParametreEntities();
    }
    
    /**
     * Get the number of Parametre Entries <br/>
     * HTTP Method: GET <br/>
     * Example URL: /parametres/count
     * @return Number of ParametreEntity
     */
    @GET
    @Path("count")
    @Produces(MediaType.APPLICATION_JSON)
    public long getCount() {
        return parametreService.countAllEntries();
    }
    
    /**
     * Get a Parametre Entity <br/>
     * HTTP Method: GET <br/>
     * Example URL: /parametres/3
     * @param id
     * @return A Parametre Entity (JSON)
     */
    @Path("{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ParametreEntity getParametreById(@PathParam("id") Long id) {
        return parametreService.find(id);
    }
    
    /**
     * Create a Parametre Entity <br/>
     * HTTP Method: POST <br/>
     * POST Request Body: New ParametreEntity (JSON) <br/>
     * Example URL: /parametres
     * @param parametre
     * @return A ParametreEntity (JSON)
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ParametreEntity addParametre(ParametreEntity parametre) {
        return parametreService.save(parametre);
    }
    
    /**
     * Update an existing Parametre Entity <br/>
     * HTTP Method: PUT <br/>
     * PUT Request Body: Updated ParametreEntity (JSON) <br/>
     * Example URL: /parametres
     * @param parametre
     * @return A ParametreEntity (JSON)
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ParametreEntity updateParametre(ParametreEntity parametre) {
        return parametreService.update(parametre);
    }
    
    /**
     * Delete an existing Parametre Entity <br/>
     * HTTP Method: DELETE <br/>
     * Example URL: /parametres/3
     * @param id
     */
    @Path("{id}")
    @DELETE
    public void deleteParametre(@PathParam("id") Long id) {
        ParametreEntity parametre = parametreService.find(id);
        parametreService.delete(parametre);
    }
    
}
