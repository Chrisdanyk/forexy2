package cd.forexy.rest;

import cd.forexy.domain.RetraitEntity;
import cd.forexy.service.RetraitService;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/retraits")
@Named
public class RetraitResource implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Inject
    private RetraitService retraitService;
    
    /**
     * Get the complete list of Retrait Entries <br/>
     * HTTP Method: GET <br/>
     * Example URL: /retraits
     * @return List of RetraitEntity (JSON)
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<RetraitEntity> getAllRetraits() {
        return retraitService.findAllRetraitEntities();
    }
    
    /**
     * Get the number of Retrait Entries <br/>
     * HTTP Method: GET <br/>
     * Example URL: /retraits/count
     * @return Number of RetraitEntity
     */
    @GET
    @Path("count")
    @Produces(MediaType.APPLICATION_JSON)
    public long getCount() {
        return retraitService.countAllEntries();
    }
    
    /**
     * Get a Retrait Entity <br/>
     * HTTP Method: GET <br/>
     * Example URL: /retraits/3
     * @param id
     * @return A Retrait Entity (JSON)
     */
    @Path("{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public RetraitEntity getRetraitById(@PathParam("id") Long id) {
        return retraitService.find(id);
    }
    
    /**
     * Create a Retrait Entity <br/>
     * HTTP Method: POST <br/>
     * POST Request Body: New RetraitEntity (JSON) <br/>
     * Example URL: /retraits
     * @param retrait
     * @return A RetraitEntity (JSON)
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public RetraitEntity addRetrait(RetraitEntity retrait) {
        return retraitService.save(retrait);
    }
    
    /**
     * Update an existing Retrait Entity <br/>
     * HTTP Method: PUT <br/>
     * PUT Request Body: Updated RetraitEntity (JSON) <br/>
     * Example URL: /retraits
     * @param retrait
     * @return A RetraitEntity (JSON)
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public RetraitEntity updateRetrait(RetraitEntity retrait) {
        return retraitService.update(retrait);
    }
    
    /**
     * Delete an existing Retrait Entity <br/>
     * HTTP Method: DELETE <br/>
     * Example URL: /retraits/3
     * @param id
     */
    @Path("{id}")
    @DELETE
    public void deleteRetrait(@PathParam("id") Long id) {
        RetraitEntity retrait = retraitService.find(id);
        retraitService.delete(retrait);
    }
    
}
