package cd.forexy.rest;

import cd.forexy.domain.MouvementEntity;
import cd.forexy.service.MouvementService;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/mouvements")
@Named
public class MouvementResource implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Inject
    private MouvementService mouvementService;
    
    /**
     * Get the complete list of Mouvement Entries <br/>
     * HTTP Method: GET <br/>
     * Example URL: /mouvements
     * @return List of MouvementEntity (JSON)
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<MouvementEntity> getAllMouvements() {
        return mouvementService.findAllMouvementEntities();
    }
    
    /**
     * Get the number of Mouvement Entries <br/>
     * HTTP Method: GET <br/>
     * Example URL: /mouvements/count
     * @return Number of MouvementEntity
     */
    @GET
    @Path("count")
    @Produces(MediaType.APPLICATION_JSON)
    public long getCount() {
        return mouvementService.countAllEntries();
    }
    
    /**
     * Get a Mouvement Entity <br/>
     * HTTP Method: GET <br/>
     * Example URL: /mouvements/3
     * @param id
     * @return A Mouvement Entity (JSON)
     */
    @Path("{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public MouvementEntity getMouvementById(@PathParam("id") Long id) {
        return mouvementService.find(id);
    }
    
    /**
     * Create a Mouvement Entity <br/>
     * HTTP Method: POST <br/>
     * POST Request Body: New MouvementEntity (JSON) <br/>
     * Example URL: /mouvements
     * @param mouvement
     * @return A MouvementEntity (JSON)
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public MouvementEntity addMouvement(MouvementEntity mouvement) {
        return mouvementService.save(mouvement);
    }
    
    /**
     * Update an existing Mouvement Entity <br/>
     * HTTP Method: PUT <br/>
     * PUT Request Body: Updated MouvementEntity (JSON) <br/>
     * Example URL: /mouvements
     * @param mouvement
     * @return A MouvementEntity (JSON)
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public MouvementEntity updateMouvement(MouvementEntity mouvement) {
        return mouvementService.update(mouvement);
    }
    
    /**
     * Delete an existing Mouvement Entity <br/>
     * HTTP Method: DELETE <br/>
     * Example URL: /mouvements/3
     * @param id
     */
    @Path("{id}")
    @DELETE
    public void deleteMouvement(@PathParam("id") Long id) {
        MouvementEntity mouvement = mouvementService.find(id);
        mouvementService.delete(mouvement);
    }
    
}
