package cd.forexy.rest;

import cd.forexy.domain.AgenceEntity;
import cd.forexy.service.AgenceService;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/agences")
@Named
public class AgenceResource implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Inject
    private AgenceService agenceService;
    
    /**
     * Get the complete list of Agence Entries <br/>
     * HTTP Method: GET <br/>
     * Example URL: /agences
     * @return List of AgenceEntity (JSON)
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<AgenceEntity> getAllAgences() {
        return agenceService.findAllAgenceEntities();
    }
    
    /**
     * Get the number of Agence Entries <br/>
     * HTTP Method: GET <br/>
     * Example URL: /agences/count
     * @return Number of AgenceEntity
     */
    @GET
    @Path("count")
    @Produces(MediaType.APPLICATION_JSON)
    public long getCount() {
        return agenceService.countAllEntries();
    }
    
    /**
     * Get a Agence Entity <br/>
     * HTTP Method: GET <br/>
     * Example URL: /agences/3
     * @param id
     * @return A Agence Entity (JSON)
     */
    @Path("{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public AgenceEntity getAgenceById(@PathParam("id") Long id) {
        return agenceService.find(id);
    }
    
    /**
     * Create a Agence Entity <br/>
     * HTTP Method: POST <br/>
     * POST Request Body: New AgenceEntity (JSON) <br/>
     * Example URL: /agences
     * @param agence
     * @return A AgenceEntity (JSON)
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public AgenceEntity addAgence(AgenceEntity agence) {
        return agenceService.save(agence);
    }
    
    /**
     * Update an existing Agence Entity <br/>
     * HTTP Method: PUT <br/>
     * PUT Request Body: Updated AgenceEntity (JSON) <br/>
     * Example URL: /agences
     * @param agence
     * @return A AgenceEntity (JSON)
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public AgenceEntity updateAgence(AgenceEntity agence) {
        return agenceService.update(agence);
    }
    
    /**
     * Delete an existing Agence Entity <br/>
     * HTTP Method: DELETE <br/>
     * Example URL: /agences/3
     * @param id
     */
    @Path("{id}")
    @DELETE
    public void deleteAgence(@PathParam("id") Long id) {
        AgenceEntity agence = agenceService.find(id);
        agenceService.delete(agence);
    }
    
}
