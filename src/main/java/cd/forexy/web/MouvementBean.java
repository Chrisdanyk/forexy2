package cd.forexy.web;

import cd.forexy.domain.AgenceEntity;
import cd.forexy.domain.MouvementEntity;
import cd.forexy.domain.MouvementType;
import cd.forexy.domain.security.UserEntity;
import cd.forexy.service.AgenceService;
import cd.forexy.service.MouvementService;
import cd.forexy.service.security.SecurityWrapper;
import cd.forexy.service.security.UserService;
import cd.forexy.web.util.MessageFactory;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceException;

@Named("mouvementBean")
@ViewScoped
public class MouvementBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger logger = Logger.getLogger(MouvementBean.class.getName());

    private List<MouvementEntity> mouvementList;

    private MouvementEntity mouvement;

    @Inject
    private MouvementService mouvementService;

    @Inject
    private UserService userService;

    @Inject
    private AgenceService agenceService;

    private List<UserEntity> allUsersList;

    private List<AgenceEntity> allAgencesList;
    private UserEntity connectedUser;

    public UserEntity getConnectedUser() {
        if (connectedUser == null) {
            connectedUser = userService.findUserByUsername(SecurityWrapper.getUsername());
        }
        return connectedUser;
    }

    public void prepareNewMouvement() {
        reset();
        this.mouvement = new MouvementEntity();
        // set any default values now, if you need
        // Example: this.mouvement.setAnything("test");
    }

    public String persist() {

        if (mouvement.getId() == null && !isPermitted("mouvement:create")) {
            return "accessDenied";
        } else if (mouvement.getId() != null && !isPermitted(mouvement, "mouvement:update")) {
            return "accessDenied";
        }

        String message;

        try {

            if (mouvement.getId() != null) {
                mouvement = mouvementService.update(mouvement);
                message = "message_successfully_updated";
            } else {
                mouvement.setDate(new Date());
                mouvement.setAgence(this.getConnectedUser().getAgence());
                mouvement = mouvementService.save(mouvement);
                message = "message_successfully_created";
            }
        } catch (OptimisticLockException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_optimistic_locking_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        } catch (PersistenceException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_save_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }

        mouvementList = null;

        FacesMessage facesMessage = MessageFactory.getMessage(message);
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);

        return null;
    }

    public String delete() {

        if (!isPermitted(mouvement, "mouvement:delete")) {
            return "accessDenied";
        }

        String message;

        try {
            mouvementService.delete(mouvement);
            message = "message_successfully_deleted";
            reset();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_delete_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
        FacesContext.getCurrentInstance().addMessage(null, MessageFactory.getMessage(message));

        return null;
    }

    public void onDialogOpen(MouvementEntity mouvement) {
        reset();
        this.mouvement = mouvement;
    }

    public void reset() {
        mouvement = null;
        mouvementList = null;

        allUsersList = null;

        allAgencesList = null;

    }

    // Get a List of all user
    public List<UserEntity> getUsers() {
        if (this.allUsersList == null) {
            this.allUsersList = userService.findAllUserEntities();
        }
        return this.allUsersList;
    }

    // Update user of the current mouvement
    public void updateUser(UserEntity user) {
        this.mouvement.setUser(user);
        // Maybe we just created and assigned a new user. So reset the allUserList.
        allUsersList = null;
    }

    // Get a List of all agence
    public List<AgenceEntity> getAgences() {
        if (this.allAgencesList == null) {
            this.allAgencesList = agenceService.findAllAgenceEntities();
        }
        return this.allAgencesList;
    }

    // Update agence of the current mouvement
    public void updateAgence(AgenceEntity agence) {
        this.mouvement.setAgence(agence);
        // Maybe we just created and assigned a new agence. So reset the allAgenceList.
        allAgencesList = null;
    }

    public SelectItem[] getTypeSelectItems() {
        SelectItem[] items = new SelectItem[MouvementType.values().length];

        int i = 0;
        for (MouvementType type : MouvementType.values()) {
            items[i++] = new SelectItem(type, getLabelForType(type));
        }
        return items;
    }

    public String getLabelForType(MouvementType value) {
        if (value == null) {
            return "";
        }
        String label = MessageFactory.getMessageString(
                "enum_label_mouvement_type_" + value);
        return label == null ? value.toString() : label;
    }

    public MouvementEntity getMouvement() {
        // Need to check for null, because some strange behaviour of f:viewParam
        // Sometimes it is setting to null
        if (this.mouvement == null) {
            prepareNewMouvement();
        }
        return this.mouvement;
    }

    public void setMouvement(MouvementEntity mouvement) {
        if (mouvement != null) {
            this.mouvement = mouvement;
        }
    }

    public List<MouvementEntity> getMouvementList() {
        if (mouvementList == null) {
            mouvementList = mouvementService.findAllMouvementEntities();
        }
        return mouvementList;
    }

    public void setMouvementList(List<MouvementEntity> mouvementList) {
        this.mouvementList = mouvementList;
    }

    public boolean isPermitted(String permission) {
        return SecurityWrapper.isPermitted(permission);
    }

    public boolean isPermitted(MouvementEntity mouvement, String permission) {

        return SecurityWrapper.isPermitted(permission);

    }

}
