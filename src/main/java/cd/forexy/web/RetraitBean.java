package cd.forexy.web;

import cd.forexy.domain.AgenceEntity;
import cd.forexy.domain.ContratEntity;
import cd.forexy.domain.RetraitEcheance;
import cd.forexy.domain.RetraitEntity;
import cd.forexy.domain.RetraitStatus;
import cd.forexy.domain.security.UserEntity;
import cd.forexy.service.AgenceService;
import cd.forexy.service.ContratService;
import cd.forexy.service.RetraitService;
import cd.forexy.service.security.SecurityWrapper;
import cd.forexy.service.security.UserService;
import cd.forexy.web.generic.GenericLazyDataModel;
import cd.forexy.web.util.MessageFactory;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceException;

@Named("retraitBean")
@ViewScoped
public class RetraitBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger logger = Logger.getLogger(RetraitBean.class.getName());

    private GenericLazyDataModel<RetraitEntity> lazyModel;

    private RetraitEntity retrait;

    @Inject
    private RetraitService retraitService;

    @Inject
    private ContratService contratService;

    @Inject
    private UserService userService;

    @Inject
    private AgenceService agenceService;

    private List<ContratEntity> allContratsList;

    private List<UserEntity> allUsersList;

    private List<AgenceEntity> allAgencesList;
    private UserEntity connectedUser;


    public void prepareNewRetrait() {
        reset();
        this.retrait = new RetraitEntity();
        // set any default values now, if you need
        // Example: this.retrait.setAnything("test");
    }

    public GenericLazyDataModel<RetraitEntity> getLazyModel() {
        if (this.lazyModel == null) {
            this.lazyModel = new GenericLazyDataModel<>(retraitService);
        }
        return this.lazyModel;
    }

    public String persist() {

        if (retrait.getId() == null && !isPermitted("retrait:create")) {
            return "accessDenied";
        } else if (retrait.getId() != null && !isPermitted(retrait, "retrait:update")) {
            return "accessDenied";
        }

        String message;

        try {

            if (retrait.getId() != null) {
                retrait = retraitService.update(retrait);
                message = "message_successfully_updated";
            } else {
                retrait.setDate(new Date());
                retrait.setUser(this.connectedUser);
                retrait.setAgence(retrait.getContrat().getAgence());
                retrait = retraitService.save(retrait);
                message = "message_successfully_created";
            }
        } catch (OptimisticLockException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_optimistic_locking_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        } catch (PersistenceException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_save_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }

        FacesMessage facesMessage = MessageFactory.getMessage(message);
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);

        return null;
    }

    public String confirm() {
        System.out.println("serialVersionUID = " + serialVersionUID);
        if (!isPermitted(retrait, "retrait:update")) {
            return "accessDenied";
        }

        String message;

        try {
            retrait.setStatus(RetraitStatus.CONFIRME);
            retraitService.update(retrait);
            message = "message_successfully_confirmed";
            reset();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_delete_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
        FacesContext.getCurrentInstance().addMessage(null, MessageFactory.getMessage(message));

        return null;
    }


    public String delete() {

        if (!isPermitted(retrait, "retrait:delete")) {
            return "accessDenied";
        }

        String message;

        try {
            retraitService.delete(retrait);
            message = "message_successfully_deleted";
            reset();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_delete_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
        FacesContext.getCurrentInstance().addMessage(null, MessageFactory.getMessage(message));

        return null;
    }

    public void onDialogOpen(RetraitEntity retrait) {
        reset();
        this.retrait = retrait;
    }

    public void reset() {
        retrait = null;

        allContratsList = null;

        allUsersList = null;

        allAgencesList = null;

    }

    // Get a List of all contrat
    public List<ContratEntity> getContrats() {
        if (this.allContratsList == null) {
            this.allContratsList = contratService.findAllContratEntities();
        }
        return this.allContratsList;
    }

    // Update contrat of the current retrait
    public void updateContrat(ContratEntity contrat) {
        this.retrait.setContrat(contrat);
        // Maybe we just created and assigned a new contrat. So reset the allContratList.
        allContratsList = null;
    }

    // Get a List of all user
    public List<UserEntity> getUsers() {
        if (this.allUsersList == null) {
            this.allUsersList = userService.findAllUserEntities();
        }
        return this.allUsersList;
    }

    // Update user of the current retrait
    public void updateUser(UserEntity user) {
        this.retrait.setUser(user);
        // Maybe we just created and assigned a new user. So reset the allUserList.
        allUsersList = null;
    }

    // Get a List of all agence
    public List<AgenceEntity> getAgences() {
        if (this.allAgencesList == null) {
            this.allAgencesList = agenceService.findAllAgenceEntities();
        }
        return this.allAgencesList;
    }

    // Update agence of the current retrait
    public void updateAgence(AgenceEntity agence) {
        this.retrait.setAgence(agence);
        // Maybe we just created and assigned a new agence. So reset the allAgenceList.
        allAgencesList = null;
    }

    public SelectItem[] getStatusSelectItems() {
        SelectItem[] items = new SelectItem[RetraitStatus.values().length];

        int i = 0;
        for (RetraitStatus status : RetraitStatus.values()) {
            items[i++] = new SelectItem(status, getLabelForStatus(status));
        }
        return items;
    }

    public String getLabelForStatus(RetraitStatus value) {
        if (value == null) {
            return "";
        }
        String label = MessageFactory.getMessageString(
                "enum_label_retrait_status_" + value);
        return label == null ? value.toString() : label;
    }

    public SelectItem[] getEcheanceSelectItems() {
        SelectItem[] items = new SelectItem[RetraitEcheance.values().length];

        int i = 0;
        for (RetraitEcheance echeance : RetraitEcheance.values()) {
            items[i++] = new SelectItem(echeance, getLabelForEcheance(echeance));
        }
        return items;
    }

    public String getLabelForEcheance(RetraitEcheance value) {
        if (value == null) {
            return "";
        }
        String label = MessageFactory.getMessageString(
                "enum_label_retrait_echeance_" + value);
        return label == null ? value.toString() : label;
    }

    public RetraitEntity getRetrait() {
        // Need to check for null, because some strange behaviour of f:viewParam
        // Sometimes it is setting to null
        if (this.retrait == null) {
            prepareNewRetrait();
        }
        return this.retrait;
    }

    public void setRetrait(RetraitEntity retrait) {
        if (retrait != null) {
            this.retrait = retrait;
        }
    }

    public boolean isPermitted(String permission) {
        return SecurityWrapper.isPermitted(permission);
    }

    public boolean isPermitted(RetraitEntity retrait, String permission) {

        return SecurityWrapper.isPermitted(permission);

    }

    public BigDecimal sumRetraitsByMonth() {
        return retraitService.sumRetraitsByMonth() == null ? BigDecimal.ZERO : retraitService.sumRetraitsByMonth();
    }

    public BigDecimal sumRetraitsByMonth(int month) {
        return retraitService.sumRetraitsByMonth(month) == null ? BigDecimal.ZERO : retraitService.sumRetraitsByMonth(month);
    }

    public UserEntity getConnectedUser() {
        String username = SecurityWrapper.getUsername();
        if (username != null) {
            this.connectedUser = userService.findUserByUsername(username);
        }
        return connectedUser;
    }
}
