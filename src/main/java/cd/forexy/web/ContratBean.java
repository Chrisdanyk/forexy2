package cd.forexy.web;

import cd.forexy.domain.*;
import cd.forexy.domain.security.UserEntity;
import cd.forexy.service.AgenceService;
import cd.forexy.service.ContratAttachmentService;
import cd.forexy.service.ContratService;
import cd.forexy.service.ParametreService;
import cd.forexy.service.security.SecurityWrapper;
import cd.forexy.service.security.UserService;
import cd.forexy.print.HeaderFooterPageEvent;
import cd.forexy.web.generic.GenericLazyDataModel;
import cd.forexy.web.util.MessageFactory;

import java.awt.image.BufferedImage;
import java.io.*;
import java.time.LocalDate;
import java.util.*;
import java.util.List;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.activation.MimetypesFileTypeMap;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceException;
import javax.servlet.http.HttpServletResponse;


import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import org.apache.commons.io.IOUtils;
import org.imgscalr.Scalr;
import org.imgscalr.Scalr.Method;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

@Named("contratBean")
@ViewScoped
public class ContratBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger logger = Logger.getLogger(ContratBean.class.getName());

    private GenericLazyDataModel<ContratEntity> lazyModel;

    private ContratEntity contrat;

    private List<ContratAttachment> contratAttachments;

    @Inject
    private ContratService contratService;

    @Inject
    private ContratAttachmentService contratAttachmentService;

    UploadedFile uploadedImage;
    byte[] uploadedImageContents;

    @Inject
    private AgenceService agenceService;

    @Inject
    private UserService userService;
    @Inject
    private ParametreService parametreService;


    private List<AgenceEntity> allAgencesList;

    private List<UserEntity> allUsersList;

    private List<AgenceEntity> allAgence2sList;
    private UserEntity connectedUser;


    public void prepareNewContrat() {
        reset();
        this.contrat = new ContratEntity();
        // set any default values now, if you need
        // Example: this.contrat.setAnything("test");
    }

    public GenericLazyDataModel<ContratEntity> getLazyModel() {
        if (this.lazyModel == null) {
            this.lazyModel = new GenericLazyDataModel<>(contratService);
        }
        return this.lazyModel;
    }

    public void setConnectedUser(UserEntity connectedUser) {
        this.connectedUser = connectedUser;
    }

    public String persist() {

        if (contrat.getId() == null && !isPermitted("contrat:create")) {
            return "accessDenied";
        } else if (contrat.getId() != null && !isPermitted(contrat, "contrat:update")) {
            return "accessDenied";
        }

        String message;

        try {

            if (this.uploadedImage != null) {
                try {

                    BufferedImage image;
                    try (InputStream in = new ByteArrayInputStream(uploadedImageContents)) {
                        image = ImageIO.read(in);
                    }
                    image = Scalr.resize(image, Method.BALANCED, 300);

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    ImageOutputStream imageOS = ImageIO.createImageOutputStream(baos);
                    Iterator<ImageWriter> imageWriters = ImageIO.getImageWritersByMIMEType(
                            uploadedImage.getContentType());
                    ImageWriter imageWriter = (ImageWriter) imageWriters.next();
                    imageWriter.setOutput(imageOS);
                    imageWriter.write(image);

                    baos.close();
                    imageOS.close();

                    logger.log(Level.INFO, "Resized uploaded image from {0} to {1}", new Object[]{uploadedImageContents.length, baos.toByteArray().length});

                    ContratImage contratImage = new ContratImage();
                    contratImage.setContent(baos.toByteArray());
                    contrat.setImage(contratImage);
                } catch (Exception e) {
                    FacesMessage facesMessage = MessageFactory.getMessage(
                            "message_upload_exception");
                    FacesContext.getCurrentInstance().addMessage(null, facesMessage);
                    FacesContext.getCurrentInstance().validationFailed();
                    return null;
                }
            }

            if (contrat.getId() != null) {
                contrat = contratService.update(contrat);
                message = "message_successfully_updated";
            } else {
                AgenceEntity agence = this.getConnectedUser().getAgence();
                contrat.setAgence(agence);
                contrat.setUser(this.getConnectedUser());
                contrat.setDate(new Date());
                contrat.setStatut(ContratStatut.ACTIF);
                contrat.setDateEcheance1(addedMonths(contrat.getDate(), 4));
                contrat.setDateEcheance2(addedMonths(contrat.getDate(), 8));
                contrat.setDateEcheance3(addedMonths(contrat.getDate(), 12));
                System.out.println("agence = " + agence);
                long maxContractNumerForAgence = contratService.getMaxContractNumerForAgence(agence);
                contrat.setContratNumber((int) (maxContractNumerForAgence + 1));

                contrat.setContratID(String.format("%04d", contrat.getContratNumber()) + "/INV/" + agence.getAgenceID().toUpperCase() + "/" + LocalDate.now().getYear());
                contrat.setRecuID("RECU-" + contrat.getContratID());
                contrat = contratService.save(contrat);
                message = "message_successfully_created";
            }
        } catch (OptimisticLockException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_optimistic_locking_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        } catch (PersistenceException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_save_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }

        FacesMessage facesMessage = MessageFactory.getMessage(message);
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);

        return null;
    }

    public String delete() {

        if (!isPermitted(contrat, "contrat:delete")) {
            return "accessDenied";
        }

        String message;

        try {
            contratService.delete(contrat);
            message = "message_successfully_deleted";
            reset();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_delete_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
        FacesContext.getCurrentInstance().addMessage(null, MessageFactory.getMessage(message));

        return null;
    }

    public void onDialogOpen(ContratEntity contrat) {
        reset();
        this.contrat = contrat;
    }

    public void reset() {
        contrat = null;

        contratAttachments = null;

        allAgencesList = null;

        allUsersList = null;

        allAgence2sList = null;

        uploadedImage = null;
        uploadedImageContents = null;

    }

    // Get a List of all agence
    public List<AgenceEntity> getAgences() {
        if (this.allAgencesList == null) {
            this.allAgencesList = agenceService.findAllAgenceEntities();
        }
        return this.allAgencesList;
    }

    // Update agence of the current contrat
    public void updateAgence(AgenceEntity agence) {
        this.contrat.setAgence(agence);
        // Maybe we just created and assigned a new agence. So reset the allAgenceList.
        allAgencesList = null;
    }

    // Get a List of all user
    public List<UserEntity> getUsers() {
        if (this.allUsersList == null) {
            this.allUsersList = userService.findAllUserEntities();
        }
        return this.allUsersList;
    }

    // Update user of the current contrat
    public void updateUser(UserEntity user) {
        this.contrat.setUser(user);
        // Maybe we just created and assigned a new user. So reset the allUserList.
        allUsersList = null;
    }

    // Get a List of all agence2
    public List<AgenceEntity> getAgence2s() {
        if (this.allAgence2sList == null) {
            this.allAgence2sList = agenceService.findAllAgenceEntities();
        }
        return this.allAgence2sList;
    }

    // Update agence2 of the current contrat
    public void updateAgence2(AgenceEntity agence) {
        this.contrat.setAgence2(agence);
        // Maybe we just created and assigned a new agence. So reset the allAgence2List.
        allAgence2sList = null;
    }

    public SelectItem[] getCiviliteSelectItems() {
        SelectItem[] items = new SelectItem[ContratCivilite.values().length];

        int i = 0;
        for (ContratCivilite civilite : ContratCivilite.values()) {
            items[i++] = new SelectItem(civilite, getLabelForCivilite(civilite));
        }
        return items;
    }

    public String getLabelForCivilite(ContratCivilite value) {
        if (value == null) {
            return "";
        }
        String label = MessageFactory.getMessageString(
                "enum_label_contrat_civilite_" + value);
        return label == null ? value.toString() : label;
    }

    public SelectItem[] getPieceSelectItems() {
        SelectItem[] items = new SelectItem[ContratPiece.values().length];

        int i = 0;
        for (ContratPiece piece : ContratPiece.values()) {
            items[i++] = new SelectItem(piece, getLabelForPiece(piece));
        }
        return items;
    }

    public String getLabelForPiece(ContratPiece value) {
        if (value == null) {
            return "";
        }
        String label = MessageFactory.getMessageString(
                "enum_label_contrat_piece_" + value);
        return label == null ? value.toString() : label;
    }

    public SelectItem[] getStatutSelectItems() {
        SelectItem[] items = new SelectItem[ContratStatut.values().length];

        int i = 0;
        for (ContratStatut statut : ContratStatut.values()) {
            items[i++] = new SelectItem(statut, getLabelForStatut(statut));
        }
        return items;
    }

    public String getLabelForStatut(ContratStatut value) {
        if (value == null) {
            return "";
        }
        String label = MessageFactory.getMessageString(
                "enum_label_contrat_statut_" + value);
        return label == null ? value.toString() : label;
    }

    public void handleImageUpload(FileUploadEvent event) {

        Iterator<ImageWriter> imageWriters = ImageIO.getImageWritersByMIMEType(
                event.getFile().getContentType());
        if (!imageWriters.hasNext()) {
            FacesMessage facesMessage = MessageFactory.getMessage(
                    "message_image_type_not_supported");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            return;
        }

        this.uploadedImage = event.getFile();
        this.uploadedImageContents = event.getFile().getContents();

        FacesMessage message = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public byte[] getUploadedImageContents() {
        if (uploadedImageContents != null) {
            return uploadedImageContents;
        } else if (contrat != null && contrat.getImage() != null) {
            contrat = contratService.lazilyLoadImageToContrat(contrat);
            return contrat.getImage().getContent();
        }
        return null;
    }

    public List<ContratAttachment> getContratAttachments() {
        if (this.contratAttachments == null && this.contrat != null && this.contrat.getId() != null) {
            // The byte streams are not loaded from database with following line. This would cost too much.
            this.contratAttachments = this.contratAttachmentService.getAttachmentsList(contrat);
        }
        return this.contratAttachments;
    }

    public void handleAttachmentUpload(FileUploadEvent event) {

        ContratAttachment contratAttachment = new ContratAttachment();

        try {
            // Would be better to use ...getFile().getContents(), but does not work on every environment
            contratAttachment.setContent(IOUtils.toByteArray(event.getFile().getInputstream()));

            contratAttachment.setFileName(event.getFile().getFileName());
            contratAttachment.setContrat(contrat);
            contratAttachmentService.save(contratAttachment);

            // set contratAttachment to null, will be refreshed on next demand
            this.contratAttachments = null;

            FacesMessage facesMessage = MessageFactory.getMessage(
                    "message_successfully_uploaded");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);

        } catch (IOException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            FacesMessage facesMessage = MessageFactory.getMessage(
                    "message_upload_exception");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
    }

    public StreamedContent getAttachmentStreamedContent(
            ContratAttachment contratAttachment) {
        if (contratAttachment != null && contratAttachment.getContent() == null) {
            contratAttachment = contratAttachmentService
                    .find(contratAttachment.getId());
        }
        return new DefaultStreamedContent(new ByteArrayInputStream(
                contratAttachment.getContent()),
                new MimetypesFileTypeMap().getContentType(contratAttachment
                        .getFileName()), contratAttachment.getFileName());
    }

    public String deleteAttachment(ContratAttachment attachment) {

        contratAttachmentService.delete(attachment);

        // set contratAttachment to null, will be refreshed on next demand
        this.contratAttachments = null;

        FacesMessage facesMessage = MessageFactory.getMessage(
                "message_successfully_deleted", "Attachment");
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        return null;
    }

    public ContratEntity getContrat() {
        // Need to check for null, because some strange behaviour of f:viewParam
        // Sometimes it is setting to null
        if (this.contrat == null) {
            prepareNewContrat();
        }
        return this.contrat;
    }

    public void setContrat(ContratEntity contrat) {
        if (contrat != null) {
            this.contrat = contrat;
        }
    }

    public boolean isPermitted(String permission) {
        return SecurityWrapper.isPermitted(permission);
    }

    public boolean isPermitted(ContratEntity contrat, String permission) {

        return SecurityWrapper.isPermitted(permission);

    }

    public double getExipirationDate() {
        String valueConfig = parametreService.getValueConfig(ParameterName.FIN_CONTRAT);
        return Double.valueOf(valueConfig);
    }

    public void printContract(ContratEntity contratEntity) {
        try {
            FacesContext fc = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse) fc.getExternalContext().getResponse();
            response.reset();
            response.setHeader("Expires", "0");
            response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
            response.setHeader("Pragma", "public");
            response.setContentType("application/pdf");
            response.addHeader("Content-Disposition", "attachment; filename=\"" + "Contract-" + contratEntity.getContratID() + ".pdf" + "\"");
            Document document = new Document(PageSize.A4, 36, 36, 90, 50);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            PdfWriter writer = PdfWriter.getInstance(document, baos);
            HeaderFooterPageEvent event = new HeaderFooterPageEvent();
            event.setContratID(contratEntity.getContratID());
            writer.setPageEvent(event);
            document.open();
            PrintBean.addContractToDocument(contratEntity, document);
            document.close();
            response.setContentLength(baos.size());
            OutputStream os = response.getOutputStream();
            baos.writeTo(os);
            os.flush();
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public BigDecimal sumContratsByMonth() {
        return contratService.sumContratsByMonth() == null ? BigDecimal.ZERO : contratService.sumContratsByMonth();
    }

    public BigDecimal sumContratsByMonth(int month) {
        return contratService.sumContratsByMonth(month) == null ? BigDecimal.ZERO : contratService.sumContratsByMonth(month);
    }

    public static Date addedMonths(Date date, int month) {
        Calendar calendar = Calendar.getInstance();
        if (date != null) {
            calendar.setTime(date);
            calendar.add(Calendar.MONTH, month);
        }
        return calendar.getTime();
    }

    public static synchronized String generateRandomID() {
        String id = "";
        while (true) {
            id = String.valueOf(new Date().getTime());
            try {
                Long.parseLong(id);
                break;
            } catch (NumberFormatException e) {
                e.printStackTrace(System.err);
            }
        }
        return id;
    }

    public long currentYear() {
        return new Date().getYear() + 1900;
    }


    public long countAllEntries() {
        return contratService.countAllEntries();
    }

    public UserEntity getConnectedUser() {
        String username = SecurityWrapper.getUsername();
        if (username != null) {
            this.connectedUser = userService.findUserByUsername(username);
        }
        return connectedUser;
    }

    public List<ContratEntity> findAllContratsByAgence() {
        List<ContratEntity> contrats = new ArrayList<>();
        String username = SecurityWrapper.getUsername();
        if (username != null) {
            contrats = contratService.findContratsByAgence(userService.findUserByUsername(username).getAgence());
        }
        return contrats;
    }

}
