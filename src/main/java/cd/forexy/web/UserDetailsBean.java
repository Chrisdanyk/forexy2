package cd.forexy.web;

import cd.forexy.domain.UserDetailsEntity;
import cd.forexy.domain.UserDetailsImage;
import cd.forexy.service.UserDetailsService;
import cd.forexy.service.security.SecurityWrapper;
import cd.forexy.web.util.MessageFactory;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceException;

import org.imgscalr.Scalr;
import org.imgscalr.Scalr.Method;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

@Named("userDetailsBean")
@ViewScoped
public class UserDetailsBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger logger = Logger.getLogger(UserDetailsBean.class.getName());
    
    private List<UserDetailsEntity> userDetailsList;

    private UserDetailsEntity userDetails;
    
    @Inject
    private UserDetailsService userDetailsService;
    
    UploadedFile uploadedImage;
    byte[] uploadedImageContents;
    
    public void prepareNewUserDetails() {
        reset();
        this.userDetails = new UserDetailsEntity();
        // set any default values now, if you need
        // Example: this.userDetails.setAnything("test");
    }

    public String persist() {

        if (userDetails.getId() == null && !isPermitted("userDetails:create")) {
            return "accessDenied";
        } else if (userDetails.getId() != null && !isPermitted(userDetails, "userDetails:update")) {
            return "accessDenied";
        }
        
        String message;
        
        try {
            
            if (this.uploadedImage != null) {
                try {

                    BufferedImage image;
                    try (InputStream in = new ByteArrayInputStream(uploadedImageContents)) {
                        image = ImageIO.read(in);
                    }
                    image = Scalr.resize(image, Method.BALANCED, 300);

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    ImageOutputStream imageOS = ImageIO.createImageOutputStream(baos);
                    Iterator<ImageWriter> imageWriters = ImageIO.getImageWritersByMIMEType(
                            uploadedImage.getContentType());
                    ImageWriter imageWriter = (ImageWriter) imageWriters.next();
                    imageWriter.setOutput(imageOS);
                    imageWriter.write(image);
                    
                    baos.close();
                    imageOS.close();
                    
                    logger.log(Level.INFO, "Resized uploaded image from {0} to {1}", new Object[]{uploadedImageContents.length, baos.toByteArray().length});
            
                    UserDetailsImage userDetailsImage = new UserDetailsImage();
                    userDetailsImage.setContent(baos.toByteArray());
                    userDetails.setImage(userDetailsImage);
                } catch (Exception e) {
                    FacesMessage facesMessage = MessageFactory.getMessage(
                            "message_upload_exception");
                    FacesContext.getCurrentInstance().addMessage(null, facesMessage);
                    FacesContext.getCurrentInstance().validationFailed();
                    return null;
                }
            }
            
            if (userDetails.getId() != null) {
                userDetails = userDetailsService.update(userDetails);
                message = "message_successfully_updated";
            } else {
                userDetails = userDetailsService.save(userDetails);
                message = "message_successfully_created";
            }
        } catch (OptimisticLockException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_optimistic_locking_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        } catch (PersistenceException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_save_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
        
        userDetailsList = null;

        FacesMessage facesMessage = MessageFactory.getMessage(message);
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        
        return null;
    }
    
    public String delete() {
        
        if (!isPermitted(userDetails, "userDetails:delete")) {
            return "accessDenied";
        }
        
        String message;
        
        try {
            userDetailsService.delete(userDetails);
            message = "message_successfully_deleted";
            reset();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_delete_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
        FacesContext.getCurrentInstance().addMessage(null, MessageFactory.getMessage(message));
        
        return null;
    }
    
    public void onDialogOpen(UserDetailsEntity userDetails) {
        reset();
        this.userDetails = userDetails;
    }
    
    public void reset() {
        userDetails = null;
        userDetailsList = null;
        
        uploadedImage = null;
        uploadedImageContents = null;
        
    }

    public void handleImageUpload(FileUploadEvent event) {
        
        Iterator<ImageWriter> imageWriters = ImageIO.getImageWritersByMIMEType(
                event.getFile().getContentType());
        if (!imageWriters.hasNext()) {
            FacesMessage facesMessage = MessageFactory.getMessage(
                    "message_image_type_not_supported");
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            return;
        }
        
        this.uploadedImage = event.getFile();
        this.uploadedImageContents = event.getFile().getContents();
        
        FacesMessage message = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
    
    public byte[] getUploadedImageContents() {
        if (uploadedImageContents != null) {
            return uploadedImageContents;
        } else if (userDetails != null && userDetails.getImage() != null) {
            userDetails = userDetailsService.lazilyLoadImageToUserDetails(userDetails);
            return userDetails.getImage().getContent();
        }
        return null;
    }
    
    public UserDetailsEntity getUserDetails() {
        // Need to check for null, because some strange behaviour of f:viewParam
                // Sometimes it is setting to null
        if (this.userDetails == null) {
            prepareNewUserDetails();
        }
        return this.userDetails;
    }
    
    public void setUserDetails(UserDetailsEntity userDetails) {
            if (userDetails != null) {
        this.userDetails = userDetails;
            }
    }
    
    public List<UserDetailsEntity> getUserDetailsList() {
        if (userDetailsList == null) {
            userDetailsList = userDetailsService.findAllUserDetailsEntities();
        }
        return userDetailsList;
    }

    public void setUserDetailsList(List<UserDetailsEntity> userDetailsList) {
        this.userDetailsList = userDetailsList;
    }
    
    public boolean isPermitted(String permission) {
        return SecurityWrapper.isPermitted(permission);
    }

    public boolean isPermitted(UserDetailsEntity userDetails, String permission) {
        
        return SecurityWrapper.isPermitted(permission);
        
    }
    
}
