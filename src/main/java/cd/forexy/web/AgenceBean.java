package cd.forexy.web;

import cd.forexy.domain.AgenceEntity;
import cd.forexy.domain.AgenceStatut;
import cd.forexy.service.AgenceService;
import cd.forexy.service.security.SecurityWrapper;
import cd.forexy.web.util.MessageFactory;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceException;

@Named("agenceBean")
@ViewScoped
public class AgenceBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger logger = Logger.getLogger(AgenceBean.class.getName());
    
    private List<AgenceEntity> agenceList;

    private AgenceEntity agence;
    
    @Inject
    private AgenceService agenceService;
    
    public void prepareNewAgence() {
        reset();
        this.agence = new AgenceEntity();
        // set any default values now, if you need
        // Example: this.agence.setAnything("test");
    }

    public String persist() {

        if (agence.getId() == null && !isPermitted("agence:create")) {
            return "accessDenied";
        } else if (agence.getId() != null && !isPermitted(agence, "agence:update")) {
            return "accessDenied";
        }
        
        String message;
        
        try {
            
            if (agence.getId() != null) {
                agence = agenceService.update(agence);
                message = "message_successfully_updated";
            } else {
                agence.setStatut(AgenceStatut.ACTIF);
                agence = agenceService.save(agence);
                message = "message_successfully_created";
            }
        } catch (OptimisticLockException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_optimistic_locking_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        } catch (PersistenceException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_save_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
        
        agenceList = null;

        FacesMessage facesMessage = MessageFactory.getMessage(message);
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        
        return null;
    }
    
    public String delete() {
        
        if (!isPermitted(agence, "agence:delete")) {
            return "accessDenied";
        }
        
        String message;
        
        try {
            agenceService.delete(agence);
            message = "message_successfully_deleted";
            reset();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_delete_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
        FacesContext.getCurrentInstance().addMessage(null, MessageFactory.getMessage(message));
        
        return null;
    }
    
    public void onDialogOpen(AgenceEntity agence) {
        reset();
        this.agence = agence;
    }
    
    public void reset() {
        agence = null;
        agenceList = null;
        
    }

    public SelectItem[] getStatutSelectItems() {
        SelectItem[] items = new SelectItem[AgenceStatut.values().length];

        int i = 0;
        for (AgenceStatut statut : AgenceStatut.values()) {
            items[i++] = new SelectItem(statut, getLabelForStatut(statut));
        }
        return items;
    }
    
    public String getLabelForStatut(AgenceStatut value) {
        if (value == null) {
            return "";
        }
        String label = MessageFactory.getMessageString(
                "enum_label_agence_statut_" + value);
        return label == null? value.toString() : label;
    }
    
    public AgenceEntity getAgence() {
        // Need to check for null, because some strange behaviour of f:viewParam
                // Sometimes it is setting to null
        if (this.agence == null) {
            prepareNewAgence();
        }
        return this.agence;
    }
    
    public void setAgence(AgenceEntity agence) {
            if (agence != null) {
        this.agence = agence;
            }
    }
    
    public List<AgenceEntity> getAgenceList() {
        if (agenceList == null) {
            agenceList = agenceService.findAllAgenceEntities();
        }
        return agenceList;
    }

    public void setAgenceList(List<AgenceEntity> agenceList) {
        this.agenceList = agenceList;
    }
    
    public boolean isPermitted(String permission) {
        return SecurityWrapper.isPermitted(permission);
    }

    public boolean isPermitted(AgenceEntity agence, String permission) {
        
        return SecurityWrapper.isPermitted(permission);
        
    }
    public long countAllEntries() {
        return agenceService.countAllEntries();
    }

    
}
