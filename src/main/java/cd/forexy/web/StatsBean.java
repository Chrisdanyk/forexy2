package cd.forexy.web;

import cd.forexy.domain.ContratEntity;
import cd.forexy.domain.RetraitEntity;
import cd.forexy.domain.security.UserEntity;
import cd.forexy.service.ContratService;
import cd.forexy.service.RetraitService;
import cd.forexy.service.security.SecurityWrapper;
import cd.forexy.service.security.UserService;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Named("statBean")
@ViewScoped
public class StatsBean implements Serializable {


    @Inject
    private ContratService contratService;

    @Inject
    private RetraitService retraitService;

    @Inject
    private UserService userService;

    private int notifNumber = 0;
    private ContratEntity contrat;

    public ContratEntity getContrat() {
        return contrat;
    }

    public void setContrat(ContratEntity contrat) {
        this.contrat = contrat;
    }

    public Long countRetraitTodayByAgence() {
        if (getConnectedUser() != null) {
            return retraitService.countallRetraitsTodayByAgence(getConnectedUser().getAgence());
        }
        return 0L;
    }

    public Double sumMoneyRetraitTodayByAgence() {
        if (getConnectedUser() != null) {
            List<RetraitEntity> retraits = retraitService.allRetraitsTodayByAgence(getConnectedUser().getAgence());
            return retraits.stream().mapToDouble(o -> (o.getMontant().doubleValue())).sum();
        }
        return null;


    }

    public Long countRetraitMonthByAgence() {
        if (getConnectedUser() != null) {
            return retraitService.countallRetraitsThisMonthByAgence(getConnectedUser().getAgence());
        }
        return 0L;
    }

    public Double sumMoneyRetraitMonthByAgence() {
        if (getConnectedUser() != null) {
            List<RetraitEntity> retraits = retraitService.allRetraitsThisMonthByAgence(getConnectedUser().getAgence());
            return retraits.stream().mapToDouble(o -> (o.getMontant().doubleValue())).sum();
        }
        return null;
    }

    public UserEntity getConnectedUser() {
        String username = SecurityWrapper.getUsername();
        if (username != null) {
            return userService.findUserByUsername(username);
        } else
            return null;
    }

    public int countContractsToday() {
        List<ContratEntity> contratEntities = contratService.allContractForToday();
        return contratEntities.size();
    }

    public int sumMoneyToday() {
        List<ContratEntity> contratEntities = contratService.allContractForToday();
        return contratEntities.stream().mapToInt(o -> (o.getMontant().intValue())).sum();
    }

    public int countContractsThisMonth() {
        List<ContratEntity> contratEntities = contratService.allContractForThisMonth();
        return contratEntities.size();
    }

    public int sumMoneyThisMonth() {
//        https://docs.oracle.com/javase/tutorial/collections/streams/reduction.html
//        https://docs.oracle.com/javase/tutorial/collections/index.html
        List<ContratEntity> contratEntities = contratService.allContractForThisMonth();
        return contratEntities.stream().mapToInt(o -> (o.getMontant().intValue())).sum();
    }

    public int getNotifNumber() {
        int x = 0;
        if (countRetraitTodayByAgence() > 1) {
            x++;
        }
        if (countRetraitMonthByAgence() > 1) {
            x++;
        }
        return x;
    }

    public void setNotifNumber(int notifNumber) {
        this.notifNumber = notifNumber;
    }

    public List<RetraitEntity> getRetraitsByAgence() {
        return retraitService.allRetraitsByAgence(getConnectedUser().getAgence());
    }

    public List<RetraitEntity> getRetraitsByAgenceToday() {
        return retraitService.allRetraitsTodayByAgence(getConnectedUser().getAgence());
    }

    public List<RetraitEntity> getRetraitsByAgenceMonth() {
        return retraitService.allRetraitsThisMonthByAgence(getConnectedUser().getAgence());
    }

    public List<RetraitEntity> retraitsByContrat() {
        return retraitService.findRetraitsByContrat(this.contrat);
    }

}
