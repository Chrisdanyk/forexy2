package cd.forexy.web;

import cd.forexy.domain.ParametreEntity;
import cd.forexy.service.ParametreService;
import cd.forexy.service.security.SecurityWrapper;
import cd.forexy.web.generic.GenericLazyDataModel;
import cd.forexy.web.util.MessageFactory;

import java.io.Serializable;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceException;

@Named("parametreBean")
@ViewScoped
public class ParametreBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger logger = Logger.getLogger(ParametreBean.class.getName());
    
    private GenericLazyDataModel<ParametreEntity> lazyModel;
    
    private ParametreEntity parametre;
    
    @Inject
    private ParametreService parametreService;
    
    public void prepareNewParametre() {
        reset();
        this.parametre = new ParametreEntity();
        // set any default values now, if you need
        // Example: this.parametre.setAnything("test");
    }

    public GenericLazyDataModel<ParametreEntity> getLazyModel() {
        if (this.lazyModel == null) {
            this.lazyModel = new GenericLazyDataModel<>(parametreService);
        }
        return this.lazyModel;
    }

    public String persist() {

        if (parametre.getId() == null && !isPermitted("parametre:create")) {
            return "accessDenied";
        } else if (parametre.getId() != null && !isPermitted(parametre, "parametre:update")) {
            return "accessDenied";
        }

        String message;

        try {

            if (parametre.getId() != null) {
                parametre = parametreService.update(parametre);
                message = "message_successfully_updated";
            } else {
                parametre.setDate(new Date());
                parametre = parametreService.save(parametre);
                message = "message_successfully_created";
            }
        } catch (OptimisticLockException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_optimistic_locking_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        } catch (PersistenceException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_save_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }

        FacesMessage facesMessage = MessageFactory.getMessage(message);
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);

        return null;
    }
    
    public String delete() {
        
        if (!isPermitted(parametre, "parametre:delete")) {
            return "accessDenied";
        }
        
        String message;
        
        try {
            parametreService.delete(parametre);
            message = "message_successfully_deleted";
            reset();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_delete_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }
        FacesContext.getCurrentInstance().addMessage(null, MessageFactory.getMessage(message));
        
        return null;
    }
    
    public void onDialogOpen(ParametreEntity parametre) {
        reset();
        this.parametre = parametre;
    }
    
    public void reset() {
        parametre = null;

    }

    public ParametreEntity getParametre() {
        // Need to check for null, because some strange behaviour of f:viewParam
                // Sometimes it is setting to null
        if (this.parametre == null) {
            prepareNewParametre();
        }
        return this.parametre;
    }
    
    public void setParametre(ParametreEntity parametre) {
            if (parametre != null) {
        this.parametre = parametre;
            }
    }
    
    public boolean isPermitted(String permission) {
        return SecurityWrapper.isPermitted(permission);
    }

    public boolean isPermitted(ParametreEntity parametre, String permission) {
        
        return SecurityWrapper.isPermitted(permission);
        
    }
    
}
