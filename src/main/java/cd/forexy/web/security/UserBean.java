package cd.forexy.web.security;

import cd.forexy.domain.AgenceEntity;
import cd.forexy.domain.UserDetailsEntity;
import cd.forexy.domain.security.UserEntity;
import cd.forexy.domain.security.UserGenre;
import cd.forexy.domain.security.UserRole;
import cd.forexy.domain.security.UserStatus;
import cd.forexy.domain.security.UserStatutMarital;
import cd.forexy.service.AgenceService;
import cd.forexy.service.UserDetailsService;
import cd.forexy.service.security.SecurityWrapper;
import cd.forexy.service.security.UserService;
import cd.forexy.web.generic.GenericLazyDataModel;
import cd.forexy.web.util.MessageFactory;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceException;

import org.primefaces.model.LazyDataModel;

@Named("userBean")
@ViewScoped
public class UserBean implements Serializable {

    private static final Logger logger = Logger.getLogger(UserBean.class.getName());

    private LazyDataModel<UserEntity> lazyModel;

    private static final long serialVersionUID = 1L;

    private UserEntity user;
    private UserEntity connectedUser;

    @Inject
    private UserService userService;

    @Inject
    private UserDetailsService userDetailsService;

    @Inject
    private AgenceService agenceService;

    private List<AgenceEntity> allAgencesList;

    private List<UserDetailsEntity> availableDetailsList;

    public UserEntity getUser() {
        if (user == null) {
            user = new UserEntity();
        }
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public void prepareNewUser() {
        this.user = new UserEntity();
        // set any default values now, if you need
    }

    public LazyDataModel<UserEntity> getLazyModel() {
        if (this.lazyModel == null) {
            this.lazyModel = new GenericLazyDataModel<>(userService);
        }
        return this.lazyModel;
    }

    public void persist() {

        String message;

        try {

            if (user.getId() != null) {
                user = userService.update(user);
                message = "message_successfully_updated";
            } else {

                // Check if a user with same username already exists
                if (userService.findUserByUsername(user.getUsername()) != null) {
                    FacesMessage facesMessage = MessageFactory.getMessage(
                            "user_username_exists");
                    facesMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext.getCurrentInstance().addMessage(null, facesMessage);
                    FacesContext.getCurrentInstance().validationFailed();
                    return;
                }
                // Check if a user with same email already exists            
                if (userService.findUserByEmail(user.getEmail()) != null) {
                    FacesMessage facesMessage = MessageFactory.getMessage(
                            "user_email_exists");
                    facesMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
                    FacesContext.getCurrentInstance().addMessage(null, facesMessage);
                    FacesContext.getCurrentInstance().validationFailed();
                    return;
                }

                user = userService.save(user);
                message = "message_successfully_created";
            }

        } catch (OptimisticLockException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_optimistic_locking_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        } catch (PersistenceException e) {
            logger.log(Level.SEVERE, "Error occured", e);
            message = "message_save_exception";
            // Set validationFailed to keep the dialog open
            FacesContext.getCurrentInstance().validationFailed();
        }

        FacesMessage facesMessage = MessageFactory.getMessage(message,
                "User");
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
    }

    public String delete() {
        userService.delete(user);
        FacesMessage facesMessage = MessageFactory.getMessage(
                "message_successfully_deleted", "User");
        FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        reset();
        return null;
    }

    public void reset() {
        user = null;

        allAgencesList = null;

        availableDetailsList = null;

    }

    public SelectItem[] getRolesSelectItems() {
        SelectItem[] items = new SelectItem[UserRole.values().length];

        int i = 0;
        for (UserRole role : UserRole.values()) {
            String label = MessageFactory.getMessageString(
                    "enumeration_label_user_roles_" + role.toString());
            items[i++] = new SelectItem(role, label == null ? role.toString() : label);
        }
        return items;
    }

    public SelectItem[] getStatusSelectItems() {
        SelectItem[] items = new SelectItem[UserStatus.values().length];

        int i = 0;
        for (UserStatus status : UserStatus.values()) {
            items[i++] = new SelectItem(status, status.toString());
        }
        return items;
    }

    // Get a List of all agence
    public List<AgenceEntity> getAgences() {
        if (this.allAgencesList == null) {
            this.allAgencesList = agenceService.findAllAgenceEntities();
        }
        return this.allAgencesList;
    }

    // Update agence of the current user
    public void updateAgence(AgenceEntity agence) {
        this.user.setAgence(agence);
        // Maybe we just created and assigned a new agence. So reset the allAgenceList.
        allAgencesList = null;
    }

    // Get a List of all details
    public List<UserDetailsEntity> getAvailableDetails() {
        if (this.availableDetailsList == null) {
            this.availableDetailsList = userDetailsService.findAvailableDetails(this.user);
        }
        return this.availableDetailsList;
    }

    // Update details of the current user
    public void updateDetails(UserDetailsEntity userDetails) {
        this.user.setDetails(userDetails);
        // Maybe we just created and assigned a new details. So reset the availableDetailsList.
        availableDetailsList = null;
    }

    public SelectItem[] getGenreSelectItems() {
        SelectItem[] items = new SelectItem[UserGenre.values().length];

        int i = 0;
        for (UserGenre genre : UserGenre.values()) {
            items[i++] = new SelectItem(genre, getLabelForGenre(genre));
        }
        return items;
    }

    public String getLabelForGenre(UserGenre value) {
        if (value == null) {
            return "";
        }
        String label = MessageFactory.getMessageString(
                "enum_label_user_genre_" + value);
        return label == null ? value.toString() : label;
    }

    public SelectItem[] getStatutMaritalSelectItems() {
        SelectItem[] items = new SelectItem[UserStatutMarital.values().length];

        int i = 0;
        for (UserStatutMarital statutMarital : UserStatutMarital.values()) {
            items[i++] = new SelectItem(statutMarital, getLabelForStatutMarital(statutMarital));
        }
        return items;
    }

    public String getLabelForStatutMarital(UserStatutMarital value) {
        if (value == null) {
            return "";
        }
        String label = MessageFactory.getMessageString(
                "enum_label_user_statutMarital_" + value);
        return label == null ? value.toString() : label;
    }

    public void loadCurrentUser() {
        String username = SecurityWrapper.getUsername();
        if (username != null) {
            this.user = this.userService.findUserByUsername(username);
        } else {
            throw new RuntimeException("Can not get authorized user name");
        }
    }

    public boolean isPermitted(String permission) {
        return SecurityWrapper.isPermitted(permission);
    }

    public UserEntity getConnectedUser() {
        String username = SecurityWrapper.getUsername();
        if (username != null) {
            this.connectedUser = userService.findUserByUsername(username);
        }
        return connectedUser;
    }

    public long countAllUsers() {
        return userService.countAllEntries();
    }
}
