package cd.forexy.web.security;

import cd.forexy.domain.security.UserEntity;
import cd.forexy.domain.security.UserRole;
import cd.forexy.service.security.SecurityWrapper;
import cd.forexy.service.security.UserService;
import cd.forexy.web.util.MessageFactory;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.web.util.SavedRequest;
import org.apache.shiro.web.util.WebUtils;

@Named
@RequestScoped
public class LoginBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private String username;
    private String password;
    private boolean remember;
    private UserEntity connectedUser;
    @Inject
    private UserService userService;

    public String login() throws IOException {

        if (!SecurityWrapper.login(username, password, remember)) {
            FacesMessage message = MessageFactory.getMessage(
                    "authentication_exception");
            FacesContext.getCurrentInstance().addMessage(null, message);
            return null;
        }

        SavedRequest savedRequest = WebUtils
                .getAndClearSavedRequest((HttpServletRequest) FacesContext
                        .getCurrentInstance().getExternalContext()
                        .getRequest());

        if (savedRequest != null) {
            FacesContext.getCurrentInstance().getExternalContext()
                    .redirect(savedRequest.getRequestUrl());
            return null;
        } else {
            return "/pages/index.xhtml?faces-redirect=true";
        }
    }

    public void logout() throws IOException {
        SecurityWrapper.logout();
        String path = FacesContext.getCurrentInstance().getExternalContext().getApplicationContextPath() + "/login.xhtml";
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        FacesContext.getCurrentInstance().getExternalContext().redirect(path);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isRemember() {
        return remember;
    }

    public void setRemember(boolean remember) {
        this.remember = remember;
    }

    public void checkAlreadyLoggedin() throws IOException {
        String username = SecurityWrapper.getUsername();
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();

        if (username != null) {
            this.connectedUser = this.userService.findUserByUsername(username);
            List<UserRole> roles = this.connectedUser.getRoles();
            if (roles.contains(UserRole.Administrator)) {
                ec.redirect(ec.getRequestContextPath() + "/pages/admin/dashboard.xhtml");
            }
            if (roles.contains(UserRole.Daf) || roles.contains(UserRole.Dg)) {
                ec.redirect(ec.getRequestContextPath() + "/pages/dashboard/dashboard.xhtml");
            }
            if (roles.contains(UserRole.Agent)) {
                ec.redirect(ec.getRequestContextPath() + "/pages/contrat/contrat.xhtml");
            }
            if (roles.contains(UserRole.AgentReception)) {
                ec.redirect(ec.getRequestContextPath() + "/pages/mouvement/mouvement.xhtml");
            }

        } else {
            throw new RuntimeException("Can not get authorized user name");
        }

    }

    public String returnDashboard() throws IOException {
        String returnUrl = null;
        String username = SecurityWrapper.getUsername();
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        if (username != null) {
            this.connectedUser = this.userService.findUserByUsername(username);
            List<UserRole> roles = this.connectedUser.getRoles();
            if (roles.contains(UserRole.Administrator)) {
                returnUrl = "/pages/admin/dashboard.xhtml";
            }
            if (roles.contains(UserRole.Daf) || roles.contains(UserRole.Dg)) {
                returnUrl = "/pages/dashboard/dashboard.xhtml";
            }
            if (roles.contains(UserRole.Agent)) {
                returnUrl = "/pages/contrat/contrat.xhtml";
            }
            if (roles.contains(UserRole.AgentReception)) {
                returnUrl = "/pages/mouvement/mouvement.xhtml";
            }

        }
//        else {
//            throw new RuntimeException("Can not get authorized user name");
//        }
        return returnUrl;
    }

    public UserEntity getConnectedUser() {
        if (SecurityWrapper.getUsername() == null) {
            this.connectedUser = userService.findUserByUsername(SecurityWrapper.getUsername());
        }
        return connectedUser;
    }
}